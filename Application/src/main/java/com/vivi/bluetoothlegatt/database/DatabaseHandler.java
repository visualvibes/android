package com.vivi.bluetoothlegatt.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

  // All Static variables
  // Database Version
  private static final int DATABASE_VERSION = 1;  // previous version 0

  // Database Name
  private static final String DATABASE_NAME = "ViviLink.db";

  public DatabaseHandler(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {

    System.out.println("ViviLink db create --> ");

    // creating required tables
    db.execSQL(TablePresetMaster.CREATE_TABLE_PRESET_MASTER);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    System.out.println("ViviLink db upgrade --> ");

    // on upgrade drop older tables
    //if (oldVersion == 1) {
    //  System.out.println("ViviLink db upgrade case 1 --> ");
    // For version 2
    //db.execSQL("ALTER TABLE " + TableRCContactRequest.TABLE_RC_CONTACT_ACCESS_REQUEST +
    //    " ADD COLUMN " + TableRCContactRequest.COLUMN_CAR_IMG + " text ");
    //
    //db.execSQL("ALTER TABLE " + TableRCContactRequest.TABLE_RC_CONTACT_ACCESS_REQUEST
    //    + " ADD COLUMN " + TableRCContactRequest.COLUMN_CAR_PROFILE_DETAILS + " " +
    //    "text ");
    //}
  }
  // create new tables
  //        onCreate(db);
}