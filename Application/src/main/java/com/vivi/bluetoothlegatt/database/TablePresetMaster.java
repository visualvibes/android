package com.vivi.bluetoothlegatt.database;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;

public class TablePresetMaster {

  private DatabaseHandler databaseHandler;

  public TablePresetMaster(DatabaseHandler databaseHandler) {
    this.databaseHandler = databaseHandler;
  }

  private static final String TABLE_PRESET_MASTER = "tbl_preset_master";

  private static final String COLUMN_PRESET_ID = "preset_id";
  private static final String COLUMN_PRESET_NAME = "preset_name";
  private static final String COLUMN_BASS_COLOR = "bass_color";
  private static final String COLUMN_BASS_BRIGHT = "bass_bright";
  private static final String COLUMN_BASS_COLOR_OPTIONS = "bass_color_options";
  private static final String COLUMN_BASS_ANIMATION_OPTION = "bass_animation_option";
  private static final String COLUMN_BASS_AUTO_ANIMATION = "bass_auto_animation";
  private static final String COLUMN_MID_COLOR = "mid_color";
  private static final String COLUMN_MID_BRIGHT = "mid_bright";
  private static final String COLUMN_MID_COLOR_OPTIONS = "mid_color_options";
  private static final String COLUMN_MID_ANIMATION_OPTION = "mid_animation_option";
  private static final String COLUMN_MID_AUTO_ANIMATION = "mid_auto_animation";
  private static final String COLUMN_TREBLE_COLOR = "treble_color";
  private static final String COLUMN_TREBLE_BRIGHT = "treble_bright";
  private static final String COLUMN_TREBLE_COLOR_OPTIONS = "treble_color_options";
  private static final String COLUMN_TREBLE_ANIMATION_OPTION = "treble_animation_option";
  private static final String COLUMN_TREBLE_AUTO_ANIMATION = "treble_auto_animation";

  static String CREATE_TABLE_PRESET_MASTER = "CREATE TABLE IF NOT EXISTS " +
      TABLE_PRESET_MASTER + " (" +
      " " + COLUMN_PRESET_ID + " integer PRIMARY KEY AUTOINCREMENT," +
      " " + COLUMN_PRESET_NAME + " text," +
      " " + COLUMN_BASS_COLOR + " integer," +
      " " + COLUMN_BASS_BRIGHT + " integer," +
      " " + COLUMN_BASS_COLOR_OPTIONS + " integer," +
      " " + COLUMN_BASS_ANIMATION_OPTION + " integer," +
      " " + COLUMN_BASS_AUTO_ANIMATION + " bool," +
      " " + COLUMN_MID_COLOR + " integer," +
      " " + COLUMN_MID_BRIGHT + " integer," +
      " " + COLUMN_MID_COLOR_OPTIONS + " integer," +
      " " + COLUMN_MID_ANIMATION_OPTION + " integer," +
      " " + COLUMN_MID_AUTO_ANIMATION + " bool," +
      " " + COLUMN_TREBLE_COLOR + " integer," +
      " " + COLUMN_TREBLE_BRIGHT + " integer," +
      " " + COLUMN_TREBLE_COLOR_OPTIONS + " integer," +
      " " + COLUMN_TREBLE_ANIMATION_OPTION + " integer," +
      " " + COLUMN_TREBLE_AUTO_ANIMATION + " bool" +
      ");";

  public ArrayList<String> getPresetName() {

    ArrayList<String> presetNameList = new ArrayList<>();

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      // Select All Query
      String selectQuery = "SELECT " + COLUMN_PRESET_NAME + " FROM " + TABLE_PRESET_MASTER;

      Cursor cursor = db.rawQuery(selectQuery, null);

      // looping through all rows and adding to list
      if (cursor.moveToFirst()) {
        do {
          presetNameList.add(cursor.getString(cursor.getColumnIndex(COLUMN_PRESET_NAME)));
        }
        while (cursor.moveToNext());

        cursor.close();
      }
    } catch (Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return presetNameList;
  }

  public PresetModel getSelectedPresetData(String presetName) {
    PresetModel presetModel = new PresetModel();

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      String selectQuery = "SELECT * FROM "
          + TABLE_PRESET_MASTER
          + " where "
          + COLUMN_PRESET_NAME
          + " = '" + presetName + "'";

      Cursor cursor = db.rawQuery(selectQuery, null);

      // looping through all rows and adding to list
      if (cursor.moveToFirst()) {
        do {

          presetModel.setPresetName(cursor.getString(cursor.getColumnIndex(COLUMN_PRESET_NAME)));

          presetModel.setBassAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_ANIMATION_OPTION)));
          presetModel.setMidAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_MID_ANIMATION_OPTION)));
          presetModel.setTrebleAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_ANIMATION_OPTION)));
        }
        while (cursor.moveToNext());

        cursor.close();
      }
    } catch (
        Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return presetModel;
  }

  public ArrayList<PresetModel> getAllPresetData(SharedPreferences sharedpreferences) {

    ArrayList<PresetModel> presetList = new ArrayList<>();

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      // Select All Query
      String selectQuery = "SELECT * FROM " + TABLE_PRESET_MASTER;

      Cursor cursor = db.rawQuery(selectQuery, null);

      // looping through all rows and adding to list
      if (cursor.moveToFirst()) {
        do {

          PresetModel presetModel = new PresetModel();

          presetModel.setId(presetList.size() + 1);
          presetModel.setPresetId(cursor.getInt(cursor.getColumnIndex(COLUMN_PRESET_ID)));
          presetModel.setPresetName(cursor.getString(cursor.getColumnIndex(COLUMN_PRESET_NAME)));

          presetModel.setBassColor(cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_COLOR)));
          presetModel.setBassBright(cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_BRIGHT)));
          presetModel.setBassColorOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_COLOR_OPTIONS)));
          presetModel.setBassAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_ANIMATION_OPTION)));
          presetModel.setBassAutoAnimation(
              cursor.getInt(cursor.getColumnIndex(COLUMN_BASS_AUTO_ANIMATION)));

          presetModel.setMidColor(cursor.getInt(cursor.getColumnIndex(COLUMN_MID_COLOR)));
          presetModel.setMidBright(cursor.getInt(cursor.getColumnIndex(COLUMN_MID_BRIGHT)));
          presetModel.setMidColorOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_MID_COLOR_OPTIONS)));
          presetModel.setMidAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_MID_ANIMATION_OPTION)));
          presetModel.setMidAutoAnimation(
              cursor.getInt(cursor.getColumnIndex(COLUMN_MID_AUTO_ANIMATION)));

          presetModel.setTrebleColor(cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_COLOR)));
          presetModel.setTrebleBright(cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_BRIGHT)));
          presetModel.setTrebleColorOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_COLOR_OPTIONS)));
          presetModel.setTrebleAnimationOptions(
              cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_ANIMATION_OPTION)));
          presetModel.setTrebleAutoAnimation(
              cursor.getInt(cursor.getColumnIndex(COLUMN_TREBLE_AUTO_ANIMATION)));

          if (sharedpreferences.contains("presetName")) {

            if (sharedpreferences.getString("presetName", "")
                .equalsIgnoreCase(cursor.getString(cursor.getColumnIndex(COLUMN_PRESET_NAME)))) {
              StaticDataUtility.presetName = sharedpreferences.getString("presetName", "");
              presetModel.setPresetOn(true);
            } else {
              presetModel.setPresetOn(false);
            }
          } else {
            presetModel.setPresetOn(false);
          }

          presetList.add(presetModel);
        }
        while (cursor.moveToNext());

        cursor.close();
      }
    } catch (
        Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return presetList;
  }

  public boolean checkPresetNameExist(String presetName) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();
    boolean isPresetNameExist = false;

    try {

      String selectQuery = "SELECT * FROM "
          + TABLE_PRESET_MASTER
          + " WHERE "
          + COLUMN_PRESET_NAME
          + "='"
          + presetName + "'";

      Cursor cursor = db.rawQuery(selectQuery, null);

      isPresetNameExist = cursor.getCount() > 0;

      cursor.close();
    } catch (Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return isPresetNameExist;
  }

  public boolean deletePreset(int presetId) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();
    boolean isDelete = false;

    try {
      isDelete = db.delete(TABLE_PRESET_MASTER, COLUMN_PRESET_ID + "=?",
          new String[] { String.valueOf(presetId) }) > 0;
    } catch (Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return isDelete;
  }

  public boolean updatePreset(int presetId, String presetName) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();
    boolean isUpdate = false;

    try {

      ContentValues dataToInsert = new ContentValues();
      dataToInsert.put(COLUMN_PRESET_NAME, presetName);

      isUpdate = db.update(TABLE_PRESET_MASTER, dataToInsert, COLUMN_PRESET_ID + "=?",
          new String[] { String.valueOf(presetId) }) > 0;
    } catch (Exception e) {
      System.out.println("ViviLink --> " + e.getLocalizedMessage());
    }

    db.close();

    return isUpdate;
  }

  public void insertAllData(ArrayList<PresetModel> presetList) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      db.execSQL("delete from " + TABLE_PRESET_MASTER);
      db.execSQL("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME = '" + TABLE_PRESET_MASTER + "'");

      for (int i = 0; i < presetList.size(); i++) {

        PresetModel presetModel = presetList.get(i);

        ContentValues values = new ContentValues();
        values.put(COLUMN_PRESET_NAME, presetModel.getPresetName());
        values.put(COLUMN_BASS_COLOR, presetModel.getBassColor());
        values.put(COLUMN_BASS_BRIGHT, presetModel.getBassBright());
        values.put(COLUMN_BASS_COLOR_OPTIONS, presetModel.getBassColorOptions());
        values.put(COLUMN_BASS_ANIMATION_OPTION, presetModel.getBassAnimationOptions());
        values.put(COLUMN_BASS_AUTO_ANIMATION, presetModel.getBassAutoAnimation());

        values.put(COLUMN_MID_COLOR, presetModel.getMidColor());
        values.put(COLUMN_MID_BRIGHT, presetModel.getMidBright());
        values.put(COLUMN_MID_COLOR_OPTIONS, presetModel.getMidColorOptions());
        values.put(COLUMN_MID_ANIMATION_OPTION, presetModel.getMidAnimationOptions());
        values.put(COLUMN_MID_AUTO_ANIMATION, presetModel.getMidAutoAnimation());

        values.put(COLUMN_TREBLE_COLOR, presetModel.getTrebleColor());
        values.put(COLUMN_TREBLE_BRIGHT, presetModel.getTrebleBright());
        values.put(COLUMN_TREBLE_COLOR_OPTIONS, presetModel.getTrebleColorOptions());
        values.put(COLUMN_TREBLE_ANIMATION_OPTION, presetModel.getTrebleAnimationOptions());
        values.put(COLUMN_TREBLE_AUTO_ANIMATION, presetModel.getTrebleAutoAnimation());

        // Inserting Row
        int what = (int) db.insert(TABLE_PRESET_MASTER, null, values);
        System.out.println("ViviLink insert --> " + what);
      }
    } catch (Exception e) {
      System.out.println("ViviLink insert --> " + e.getLocalizedMessage());
    }

    db.close();
  }

  public void insertData(PresetModel presetModel) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      ContentValues values = new ContentValues();
      values.put(COLUMN_PRESET_NAME, presetModel.getPresetName());
      values.put(COLUMN_BASS_COLOR, presetModel.getBassColor());
      values.put(COLUMN_BASS_BRIGHT, presetModel.getBassBright());
      values.put(COLUMN_BASS_COLOR_OPTIONS, presetModel.getBassColorOptions());
      values.put(COLUMN_BASS_ANIMATION_OPTION, presetModel.getBassAnimationOptions());
      values.put(COLUMN_BASS_AUTO_ANIMATION, presetModel.getBassAutoAnimation());

      values.put(COLUMN_MID_COLOR, presetModel.getMidColor());
      values.put(COLUMN_MID_BRIGHT, presetModel.getMidBright());
      values.put(COLUMN_MID_COLOR_OPTIONS, presetModel.getMidColorOptions());
      values.put(COLUMN_MID_ANIMATION_OPTION, presetModel.getMidAnimationOptions());
      values.put(COLUMN_MID_AUTO_ANIMATION, presetModel.getMidAutoAnimation());

      values.put(COLUMN_TREBLE_COLOR, presetModel.getTrebleColor());
      values.put(COLUMN_TREBLE_BRIGHT, presetModel.getTrebleBright());
      values.put(COLUMN_TREBLE_COLOR_OPTIONS, presetModel.getTrebleColorOptions());
      values.put(COLUMN_TREBLE_ANIMATION_OPTION, presetModel.getTrebleAnimationOptions());
      values.put(COLUMN_TREBLE_AUTO_ANIMATION, presetModel.getTrebleAutoAnimation());

      // Inserting Row
      int what = (int) db.insert(TABLE_PRESET_MASTER, null, values);
      System.out.println("ViviLink insert --> " + what);
    } catch (Exception e) {
      System.out.println("ViviLink insert --> " + e.getLocalizedMessage());
    }

    db.close();
  }

  public void updatePresetData(PresetModel presetModel) {

    SQLiteDatabase db = databaseHandler.getWritableDatabase();

    try {

      ContentValues values = new ContentValues();
      values.put(COLUMN_PRESET_NAME, presetModel.getPresetName());
      values.put(COLUMN_BASS_COLOR, presetModel.getBassColor());
      values.put(COLUMN_BASS_BRIGHT, presetModel.getBassBright());
      values.put(COLUMN_BASS_COLOR_OPTIONS, presetModel.getBassColorOptions());
      values.put(COLUMN_BASS_ANIMATION_OPTION, presetModel.getBassAnimationOptions());
      values.put(COLUMN_BASS_AUTO_ANIMATION, presetModel.getBassAutoAnimation());

      values.put(COLUMN_MID_COLOR, presetModel.getMidColor());
      values.put(COLUMN_MID_BRIGHT, presetModel.getMidBright());
      values.put(COLUMN_MID_COLOR_OPTIONS, presetModel.getMidColorOptions());
      values.put(COLUMN_MID_ANIMATION_OPTION, presetModel.getMidAnimationOptions());
      values.put(COLUMN_MID_AUTO_ANIMATION, presetModel.getMidAutoAnimation());

      values.put(COLUMN_TREBLE_COLOR, presetModel.getTrebleColor());
      values.put(COLUMN_TREBLE_BRIGHT, presetModel.getTrebleBright());
      values.put(COLUMN_TREBLE_COLOR_OPTIONS, presetModel.getTrebleColorOptions());
      values.put(COLUMN_TREBLE_ANIMATION_OPTION, presetModel.getTrebleAnimationOptions());
      values.put(COLUMN_TREBLE_AUTO_ANIMATION, presetModel.getTrebleAutoAnimation());

      // Inserting Row
      int what = db.update(TABLE_PRESET_MASTER, values, COLUMN_PRESET_ID + "=?",
          new String[] { String.valueOf(presetModel.getPresetId()) });
      System.out.println("ViviLink update --> " + what);
    } catch (Exception e) {
      System.out.println("ViviLink update --> " + e.getLocalizedMessage());
    }

    db.close();
  }
}

//"CREATE TABLE IF NOT EXISTS tbl_preset_master (id INTEGER, preset_name TEXT,"
//    + " bass_color INTEGER, bass_bright INTEGER, bass_color_options INTEGER, "
//    + "bass_animation_option INTEGER,bass_auto_animation BOOL, mid_color INTEGER,"
//    + "mid_bright INTEGER, mid_color_options INTEGER, mid_animation_option INTEGER,"
//    + "mid_auto_animation BOOL, treble_color INTEGER, treble_bright INTEGER,"
//    + " treble_color_options INTEGER, treble_animation_option INTEGER,treble_auto_animation BOOL)"
