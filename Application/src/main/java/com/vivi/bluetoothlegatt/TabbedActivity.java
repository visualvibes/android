package com.vivi.bluetoothlegatt;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import com.google.gson.Gson;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.preset.PresetListActivity;
import com.vivi.bluetoothlegatt.utils.NonSwappableViewPager;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class TabbedActivity extends FragmentActivity implements ActionBar.TabListener {

  private final static String TAG = "ViviLink Tabbed ";

  // private final String LIST_NAME = "NAME";
  // private final String LIST_UUID = "UUID";
  private TextView mConnectionState;
  private String connectionState;
  private String mDeviceAddress;

  @SuppressLint("StaticFieldLeak")
  private static Activity activity;
  private static SharedPreferences sharedpreferences;
  private static SharedPreferences.Editor prefsEditor;
  private static PresetModel tempPreset = new PresetModel();

  //private boolean mConnected = false;
  // private BluetoothGattCharacteristic mNotifyCharacteristic;
  public static BluetoothGattCharacteristic txChar;
  private static BluetoothGattCharacteristic rxChar;
  private static BluetoothGattCharacteristic enableChar;
  private static BluetoothGattCharacteristic ctrlChar;
  private static BluetoothGattCharacteristic flowChar;
  private static BluetoothGattCharacteristic baudChar;
  //fifos for each characteristic used
  private static ArrayList<byte[]> ctrlfifo;
  public static ArrayList<byte[]> txfifo;
  // private ArrayList<byte[]> rxfifo;
  private static ArrayList<byte[]> enablefifo;
  private static ArrayList<byte[]> flowfifo;
  private static ArrayList<byte[]> baudfifo;

  private SectionsPagerAdapter mSectionsPagerAdapter;

  private int viewPagerPosition = 0;

  /**
   * The {@link ViewPager} that will host the section contents.
   */
  private NonSwappableViewPager mViewPager;
  private TextView mDataField;
  private String mDeviceName;
  //private ExpandableListView mGattServicesList;
  public static BluetoothLeService mBluetoothLeService;
  // Code to manage Service lifecycle.

  @Override
  protected void onDestroy() {
    super.onDestroy();
    System.out.println(TAG + "onDestroy");
    unregisterReceiver(mGattUpdateReceiver);
    unbindService(mServiceConnection);
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    resetData();
  }

  private void resetData() {
    mBluetoothLeService = null;
    activity = null;
    sharedpreferences = null;
    prefsEditor = null;
    tempPreset = null;
    txChar = null;
    rxChar = null;
    enableChar = null;
    ctrlChar = null;
    flowChar = null;
    baudChar = null;
    ctrlfifo = null;
    txfifo = null;
    enablefifo = null;
    flowfifo = null;
    baudfifo = null;

    System.gc();
  }

  private final ServiceConnection mServiceConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
      mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
      if (!mBluetoothLeService.initialize()) {
        Log.e(TAG, "Unable to initialize Bluetooth");
        finish();
      }
      // Automatically connects to the device upon successful start-up initialization.
      mBluetoothLeService.connect(mDeviceAddress);
      Log.i(TAG, "connecting again to device " + mDeviceName);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
      mBluetoothLeService = null;
      displayData("Connection Failed");
    }
  };

  // Handles various events fired by the Service.
  // ACTION_GATT_CONNECTED: connected to a GATT server.
  // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
  // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
  // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
  //                        or notification operations.
  private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();
      if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
        //mConnected = true;
        updateConnectionState();
        invalidateOptionsMenu();
      } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
        //mConnected = false;
        //updateConnectionState(R.string.disconnected);
        invalidateOptionsMenu();
        clearUI();
        updateConnState("Disconnected");
      } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
        // Show all the supported services and characteristics on the user interface.
        updateConnState("Getting Services...");
      } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
        displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

        // byte[] packet = intent.getStringExtra(BluetoothLeService.EXTRA_DATA).getBytes();

        System.out.println(
            TAG + "received:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
      } else if (BluetoothLeService.ACTION_DATA_TX.equals(action)) {
        System.out.println(TAG + "tx complete");
        nextBleMsg();
      }
    }
  };
  //saved values
  private int basscolor = 0; //element 4
  private int bassbrightness = 0; //7
  private int bassoptions = 0; //10
  private int bassanimation = 0; //1
  private int bassauto = 0; //11
  private int midcolor = 0; //5
  private int midbrightness = 0; //8
  private int midoptions = 0; //12
  private int midanimation = 0; //2
  private int midauto = 0; //13
  private int treblecolor = 0; //6
  private int treblebrightness = 0; //9
  private int trebleoptions = 0; //14
  private int trebleanimation = 0; //3
  private int trebleauto = 0; //15
  private int filter = 0; //0
  private int treblecount = 0; //21
  private int midcount = 0; //19
  private int basscount = 0; //17

  private ArrayList<Fragment> mFragments = new ArrayList<>();

  private static Drawable scaleDrawable(Context context, Drawable image, Integer width) {
    if (width == 0) {
      return image;
    }
    if (!(image instanceof BitmapDrawable)) {
      return image;
    }
    Bitmap b = ((BitmapDrawable) image).getBitmap();
    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, width, 1, false);
    image = new BitmapDrawable(context.getResources(), bitmapResized);
    return image;
  }

  private static IntentFilter makeGattUpdateIntentFilter() {
    final IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_TX);
    return intentFilter;
  }

  public void onBackPressed() {
    System.out.println(TAG + "go back to home screen");
    mBluetoothLeService.disconnect();
    resetData();
    finish();
  }

  //look at ctrlFifo and txFifo, send from ctrlfifo or if it is empty send from txfifo
  public static void nextBleMsg() {

    activity.runOnUiThread(() -> {

      if (mBluetoothLeService == null) {
        Log.e(TAG, "mBluetoothLeService null");
        return;
      }
      if (txChar == null) {
        Log.e(TAG, "txChar is null, searching for chars");
        searchGattServices(mBluetoothLeService.getSupportedGattServices());
      }
      byte[] thismessage;
      if (flowfifo.size() > 0) {
        thismessage = flowfifo.remove(0);
        mBluetoothLeService.writeChar(flowChar, thismessage);
        System.out.println(TAG + "write to flowChar");
      } else if (baudfifo.size() > 0) {
        thismessage = baudfifo.remove(0);
        mBluetoothLeService.writeChar(baudChar, thismessage);
        System.out.println(TAG + "write to baudChar");
      } else if (enablefifo.size() > 0) {
        thismessage = enablefifo.remove(0);
        mBluetoothLeService.writeChar(enableChar, thismessage);
        System.out.println(TAG + "write to enableChar");
      } else if (ctrlfifo.size() > 0) {
        thismessage = ctrlfifo.remove(0);
        mBluetoothLeService.writeChar(ctrlChar, thismessage);
        System.out.println(TAG + "write to ctrlChar");
      } else if (txfifo.size() > 0) {
        thismessage = txfifo.remove(0);
        mBluetoothLeService.writeChar(txChar, thismessage);
        System.out.println(TAG + "write to txChar");
      } else {
        System.out.println(TAG + "ble xmit done");
        return;
        //thismessage = "no message".getBytes();
      }
      System.out.println(TAG + "nextBleMsg:" + Arrays.toString(thismessage));
    });
  }

  public static void searchGattServices(List<BluetoothGattService> gattServices) {
    if (gattServices == null) return;
    String uuid;
    // Loops through available GATT Services.
    for (BluetoothGattService gattService : gattServices) {
      uuid = gattService.getUuid().toString();
      System.out.println(TAG + "service uuid " + uuid);

      List<BluetoothGattCharacteristic> gattCharacteristics =
          gattService.getCharacteristics();

      // Loops through available Characteristics.
      for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
        uuid = gattCharacteristic.getUuid().toString();
        System.out.println(TAG + "chara uuid " + uuid);

        if (uuid.equals(SampleGattAttributes.UART_RX)) {
          rxChar = gattCharacteristic;
          System.out.println(TAG + "found rx characteristic");
          mBluetoothLeService.setCharacteristicNotification(rxChar, true);
        }
        if (uuid.equals(SampleGattAttributes.UART_TX)) {
          txChar = gattCharacteristic;
          System.out.println(TAG + "found tx characteristic");
          txChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        }
        if (uuid.equals(SampleGattAttributes.UART_ENABLE)) {
          enableChar = gattCharacteristic;
          enableChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          System.out.println(TAG + "found enable characteristic");
        }
        if (uuid.equals(SampleGattAttributes.CTRL_POINT)) {
          ctrlChar = gattCharacteristic;
          ctrlChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
          System.out.println(TAG + "found control point characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_FLOW)) {
          flowChar = gattCharacteristic;
          flowChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          System.out.println(TAG + "found flow characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_BAUD)) {
          baudChar = gattCharacteristic;
          baudChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          System.out.println(TAG + "found baud characteristic");
        }
      }
    }

    if (rxChar != null
        && txChar != null
        && enableChar != null
        && ctrlChar != null
        && flowChar != null
        && baudChar != null) {
      Log.i(TAG, "chars found ok");
    } else {
      String missing = String.format(Locale.getDefault(),
          "rxchar: %d, txchar: %d, enablechar: %d, ctrlchar: %d, flowchar: %d, baudchar: %d",
          rxChar == null ? 0 : 1, txChar == null ? 0 : 1, enableChar == null ? 0 : 1,
          ctrlChar == null ? 0 : 1, flowChar == null ? 0 : 1, baudChar == null ? 0 : 1);
      Log.e(TAG, "missing chars:" + missing);
    }
  }

  private void clearUI() {
    //mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
    if (mDataField != null) {
      mDataField.setText(R.string.no_data);
    }
  }

  @SuppressLint("CommitPrefEdits")
  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.i(TAG, "onCreate top");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tabbed);

    activity = TabbedActivity.this;
    sharedpreferences = getSharedPreferences(StaticDataUtility.myPreference,
        Context.MODE_PRIVATE);
    prefsEditor = sharedpreferences.edit();

    getPrefData();

    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    // Set up the ViewPager with the sections adapter.
    mViewPager = findViewById(R.id.container);
    mViewPager.setOffscreenPageLimit(0);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    // Set up the action bar.
    final ActionBar actionBar = getActionBar();
    if (actionBar != null) {
      actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    }

    // When swiping between different sections, select the corresponding
    // tab. We can also use ActionBar.Tab#select() to do this if we have
    // a reference to the Tab.
    mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      @Override
      public void onPageSelected(int position) {
        System.out.println(TAG + "tab change to " + position);

        RangeSettingsFragment frag;

        switch (position) {
          case 0:
            break;
          case 1:
            Log.i(TAG, String.format(Locale.getDefault(), "bass brightness %d", bassbrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);
            if (frag.bbar != null) {
              Log.i(TAG, "bass brightness setting restored to " + bassbrightness);
              frag.bbar.setProgress(bassbrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(basscolor);
            }
            break;
          case 2:
            Log.i(TAG, String.format(Locale.getDefault(), "mid brightness %d", midbrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);

            if (frag.bbar != null) {
              Log.i(TAG, "mid brightness setting restored to " + midbrightness);
              frag.bbar.setProgress(midbrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(midcolor);
            }
            break;
          case 3:
            Log.i(TAG,
                String.format(Locale.getDefault(), "treble brightness %d", treblebrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);
            if (frag.bbar != null) {
              Log.i(TAG, "treble brightness setting restored to " + treblebrightness);
              frag.bbar.setProgress(treblebrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(treblecolor);
            }

            break;
          case 4:
                        /*LedSetupFragment frag;
                        frag = (LedSetupFragment)mSectionsPagerAdapter.getItem(4); */

            break;
          default:

            break;
        }
        if (actionBar != null) {
          actionBar.setSelectedNavigationItem(position);
        }
      }
    });

    // For each of the sections in the app, add a tab to the action bar.
    for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
      // Create a tab with text corresponding to the page title defined by
      // the adapter. Also specify this Activity object, which implements
      // the TabListener interface, as the callback (listener) for when
      // this tab is selected.
      if (actionBar != null) {
        actionBar.addTab(
            actionBar.newTab()
                .setText(mSectionsPagerAdapter.getPageTitle(i))
                .setTabListener(this));
      }
    }

    final Intent intent = getIntent();
    mDeviceName = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_NAME);
    mDeviceAddress = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_ADDRESS);
    filter = intent.getIntExtra("filter", 0);
    basscolor = intent.getIntExtra("basscolor", 0);
    bassbrightness = intent.getIntExtra("bassbrightness", 0);
    bassoptions = intent.getIntExtra("bassoptions", 0);
    System.out.println(TAG + "bassbrightness "
        + bassbrightness
        + " basscolor "
        + basscolor
        + "bassoptions "
        + bassoptions);
    bassanimation = intent.getIntExtra("bassanimation", 0) - 9;
    bassauto = intent.getIntExtra("bassauto", 0);
    midcolor = intent.getIntExtra("midcolor", 0);
    midbrightness = intent.getIntExtra("midbrightness", 0);
    midoptions = intent.getIntExtra("midoptions", 0);
    midanimation = intent.getIntExtra("midanimation", 0) - 9;
    midauto = intent.getIntExtra("midauto", 0);
    treblecolor = intent.getIntExtra("treblecolor", 0);
    treblebrightness = intent.getIntExtra("treblebrightness", 0);
    trebleoptions = intent.getIntExtra("trebleoptions", 0);
    System.out.println(TAG + "trebleoptions " + trebleoptions);
    trebleanimation = intent.getIntExtra("trebleanimation", 0) - 9;
    trebleauto = intent.getIntExtra("trebleauto", 0);
    basscount = intent.getIntExtra("basscount", 0);
    //16
    // t bassprotocol = intent.getIntExtra("bassprotocol", 0);
    midcount = intent.getIntExtra("midcount", 0);
    //18
    // int midprotocol = intent.getIntExtra("midprotocol", 0);
    treblecount = intent.getIntExtra("treblecount", 0);
    //20
    // int trebleprotocol = intent.getIntExtra("trebleprotocol", 0);

    //mConnectionState = (TextView) findViewById(R.id.connection_state);
    //mDataField = (TextView) findViewById(R.id.data_value);

    Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    ctrlfifo = new ArrayList<>();
    txfifo = new ArrayList<>();
    // rxfifo = new ArrayList<>();
    enablefifo = new ArrayList<>();
    flowfifo = new ArrayList<>();
    baudfifo = new ArrayList<>();

    updateConnState("Ready");
    //searchGattServices(mBluetoothLeService.getSupportedGattServices());
    Log.i(TAG, "oncreate end " + mDeviceName + " " + mDeviceAddress);

    registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
        new IntentFilter("updateValue"));
  }

  private void getPrefData() {

    Gson gson = new Gson();
    String json = sharedpreferences.getString(StaticDataUtility.CURRENT_PRESET_MODEL,
        "");

    if (!json.equalsIgnoreCase("")) {
      tempPreset = gson.fromJson(json, PresetModel.class);
    }
  }

  private static void updatePrefData() {

    TabbedActivity tabbedactivity = (TabbedActivity) activity;
    Gson gson = new Gson();

    if (tabbedactivity.bassanimation == 20) {
      tempPreset.setBassAnimationOptions(20);
    }

    if (tabbedactivity.midanimation == 20) {
      tempPreset.setMidAnimationOptions(20);
    }

    if (tabbedactivity.trebleanimation == 20) {
      tempPreset.setTrebleAnimationOptions(20);
    }

    if (tabbedactivity.bassauto == 0) {
      tempPreset.setBassAutoAnimation(0);
    }

    if (tabbedactivity.midauto == 0) {
      tempPreset.setMidAutoAnimation(0);
    }

    if (tabbedactivity.trebleauto == 0) {
      tempPreset.setTrebleAutoAnimation(0);
    }

    String json = gson.toJson(tempPreset);
    prefsEditor.putString(StaticDataUtility.CURRENT_PRESET_MODEL, json);
    prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "yes");
    prefsEditor.apply();
  }

  @Override
  protected void onResume() {
    super.onResume();
    System.out.println(TAG + "onResume");
    if (mBluetoothLeService != null) {
      final boolean result = mBluetoothLeService.connect(mDeviceAddress);
      System.out.println(TAG + "Connect request result=" + result);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    System.out.println(TAG + "onPause()");
  }

  private void updateConnectionState() {
    runOnUiThread(() -> {
      connectionState = getResources().getString(R.string.connected);
      if (mConnectionState != null) {
        mConnectionState.setText(R.string.connected);
      }
    });
  }

  //private void updateConnectionState(final int resourceId) {
  //  runOnUiThread(() -> {
  //    connectionState = getResources().getString(resourceId);
  //    if (mConnectionState != null) {
  //      mConnectionState.setText(resourceId);
  //    }
  //  });
  //}

  private void updateConnState(final String val) {
    runOnUiThread(() -> {
      connectionState = val;
      System.out.println(TAG + "connstate " + connectionState);
      if (mConnectionState != null) {
        mConnectionState.setText(val);
      }
    });
  }

  private void displayData(String data) {
    if (data != null && mDataField != null) {
      mDataField.setText(data);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_tabbed, menu);

    // Preset
    final MenuItem presetItem = menu.findItem(R.id.menu_preset);
    presetItem.setShowAsAction(
        MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

    View view = presetItem.getActionView();

    Button btnPreset = view.findViewById(R.id.btnPreset);
    btnPreset.setOnClickListener(v -> onOptionsItemSelected(presetItem));

    //MenuItem presetItem = menu.findItem(R.id.menu_preset);
    //presetItem.setActionView(
    //    R.layout.actionbar_add_preset);
    //
    //View view = presetItem.getActionView();
    //view.setPadding(2, 1, 15, 1);
    //
    //view.setOnClickListener(v -> onOptionsItemSelected(presetItem));

    return true;
  }

  @SuppressWarnings("unused")
  public static int dp2px(int dp) {
    float density = Resources.getSystem().getDisplayMetrics().density;
    return Math.round(dp * density);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    if (item.getItemId() == R.id.menu_preset) {
      viewPagerPosition = mViewPager.getCurrentItem();
      // StaticDataUtility.presetName = "";
      Intent intent = new Intent(this, PresetListActivity.class);
      intent.putExtra("mDeviceAddress", mDeviceAddress);
      intent.putExtra("mDeviceName", mDeviceName);

      intent.putExtra("basscolor", basscolor);
      intent.putExtra("bassbrightness", bassbrightness);
      intent.putExtra("bassoptions", bassoptions);
      intent.putExtra("bassanimation", bassanimation);
      intent.putExtra("bassauto", bassauto);
      intent.putExtra("midcolor", midcolor);
      intent.putExtra("midbrightness", midbrightness);
      intent.putExtra("midoptions", midoptions);
      intent.putExtra("midanimation", midanimation);
      intent.putExtra("midauto", midauto);
      intent.putExtra("treblecolor", treblecolor);
      intent.putExtra("treblebrightness", treblebrightness);
      intent.putExtra("trebleoptions", trebleoptions);
      intent.putExtra("trebleanimation", trebleanimation);
      intent.putExtra("trebleauto", trebleauto);
      intent.putExtra("filter", filter);
      intent.putExtra("treblecount", treblecount);

      startActivity(intent);
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    // When the given tab is selected, switch to the corresponding page in
    // the ViewPager.
    mViewPager.setCurrentItem(tab.getPosition());
  }

  @Override
  public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
  }

  @Override
  public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
  }

  public static class LedSetupFragment extends Fragment {
    Integer basscount;
    Integer midcount;
    Integer treblecount;
    Integer filter;

    public LedSetupFragment() {

    }

    static LedSetupFragment newInstance(Context c) {
      TabbedActivity tabbedactivity = (TabbedActivity) c;
      LedSetupFragment fragment = new LedSetupFragment();
      Bundle args = new Bundle();
      args.putInt("basscount", tabbedactivity.basscount);
      args.putInt("midcount", tabbedactivity.midcount);
      args.putInt("treblecount", tabbedactivity.treblecount);
      args.putInt("filter", tabbedactivity.filter);
      fragment.setArguments(args);
      return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      System.out.println(TAG + "create settings fragment");
      basscount = getArguments().getInt("basscount");
      midcount = getArguments().getInt("midcount");
      treblecount = getArguments().getInt("treblecount");
      filter = getArguments().getInt("filter");
      System.out.println(TAG +
          "basscount " + basscount + " midcount " + midcount + " treblecount " + treblecount);
      View rootView = inflater.inflate(R.layout.fragment_ledsetup, container, false);

      final TextView filterLabel = rootView.findViewById(R.id.filterLabel);
      filterLabel.setText(String.format(Locale.getDefault(), "Noise Filter: %d", filter));

      final SeekBar filterSeekBar = rootView.findViewById(R.id.filterSeekBar);
      filterSeekBar.setProgress(filter);
      filterSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          System.out.println(TAG + "filter changed to " + seekbar.getProgress());
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          tabbedactivity.filter = seekbar.getProgress();
          txfifo.add(new byte[] { (byte) 0x09, 0, (byte) seekbar.getProgress() });
          nextBleMsg();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
          if (fromUser) {
            if (progress < 1) {
              seekBar.setProgress(1);
            }
            filterLabel.setText(
                String.format(Locale.getDefault(), "Noise Filter: %d", seekBar.getProgress()));
          }
        }
      });

      final TextView trebleCountLabel = rootView.findViewById(R.id.trebleCountLabel);

      final SeekBar trebleCountSeekBar = rootView.findViewById(R.id.trebleCountSeekBar);
      trebleCountSeekBar.setProgress(treblecount);
      trebleCountLabel.setText(
          String.format(Locale.getDefault(), "LED 3 - Treble - # of LEDs: %d", treblecount));
      trebleCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          System.out.println(TAG + "treble count changed to " + seekbar.getProgress());
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          tabbedactivity.treblecount = seekbar.getProgress();
          txfifo.add(new byte[] {
              (byte) 39, (byte) ((seekbar.getProgress() >> 8) & 0xff),
              (byte) (seekbar.getProgress() & 0xff)
          });
          nextBleMsg();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
          trebleCountLabel.setText(
              String.format(Locale.getDefault(), "LED 3 - Treble - # of LEDs: %d", progress));
        }
      });
      final Button threeCountMinusButton =
          rootView.findViewById(R.id.threeCountMinusButton);
      threeCountMinusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out3 treble count minus button pressed");
        if (trebleCountSeekBar.getProgress() > 2) {
          trebleCountSeekBar.setProgress(trebleCountSeekBar.getProgress() - 1);
          trebleCountLabel.setText(
              String.format(Locale.getDefault(), "LED 3 - Treble - # of LEDs: %d",
                  trebleCountSeekBar.getProgress()));
        }
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 39, (byte) ((trebleCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (trebleCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      final Button threeCountPlusButton = rootView.findViewById(R.id.threeCountPlusButton);
      threeCountPlusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out3 treble count plus button pressed");
        trebleCountSeekBar.setProgress(trebleCountSeekBar.getProgress() + 1);
        trebleCountLabel.setText(
            String.format(Locale.getDefault(), "LED 3 - Treble - # of LEDs: %d",
                trebleCountSeekBar.getProgress()));
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 39, (byte) ((trebleCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (trebleCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      //            final Spinner trebleprotocol = (Spinner)rootView.findViewById(R.id.trebleProtocol);
      //            trebleprotocol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      //                @Override
      //                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
      //                    System.out.println(TAG + "treble protocol selected " + position);
      //                    //TabbedActivity tabbedactivity = (TabbedActivity)getActivity();
      //                    //tabbedactivity.txfifo.add(new byte[]{(byte)( (rangenum*10) + 7), 0, (byte)position});
      //                    //tabbedactivity.nextBleMsg();
      //                }
      //
      //                @Override
      //                public void onNothingSelected(AdapterView<?> parentView) {
      //
      //                }
      //
      //            });

      final TextView midCountLabel = rootView.findViewById(R.id.midCountLabel);
      midCountLabel.setText(
          String.format(Locale.getDefault(), "LED 2 - Mids - # of LEDs: %d", midcount));

      final SeekBar midCountSeekBar = rootView.findViewById(R.id.midCountSeekBar);
      midCountSeekBar.setProgress(midcount);
      midCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          System.out.println(TAG + "mid count changed to " + seekbar.getProgress());
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          tabbedactivity.midcount = seekbar.getProgress();
          txfifo.add(new byte[] {
              (byte) 29, (byte) ((seekbar.getProgress() >> 8) & 0xff),
              (byte) (seekbar.getProgress() & 0xff)
          });
          nextBleMsg();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
          midCountLabel.setText(
              String.format(Locale.getDefault(), "LED 2 - Mids - # of LEDs: %d", progress));
        }
      });

      final Button twoCountMinusButton = rootView.findViewById(R.id.twoCountMinusButton);
      twoCountMinusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out2 mids count minus button pressed");
        if (midCountSeekBar.getProgress() > 2) {
          midCountSeekBar.setProgress(midCountSeekBar.getProgress() - 1);
          midCountLabel.setText(
              String.format(Locale.getDefault(), "LED 2 - Mids - # of LEDs: %d",
                  midCountSeekBar.getProgress()));
        }
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 29, (byte) ((midCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (midCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      final Button twoCountPlusButton = rootView.findViewById(R.id.twoCountPlusButton);
      twoCountPlusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out2 mids count plus button pressed");
        midCountSeekBar.setProgress(midCountSeekBar.getProgress() + 1);
        midCountLabel.setText(
            String.format(Locale.getDefault(), "LED 2 - Mids - # of LEDs: %d",
                midCountSeekBar.getProgress()));
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 29, (byte) ((midCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (midCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      //            final Spinner midprotocol = (Spinner)rootView.findViewById(R.id.midProtocol);
      //            midprotocol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      //                @Override
      //                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
      //                    System.out.println(TAG + "mid protocol selected " + position);
      //                    //TabbedActivity tabbedactivity = (TabbedActivity)getActivity();
      //                    //tabbedactivity.txfifo.add(new byte[]{(byte)( (rangenum*10) + 7), 0, (byte)position});
      //                    //tabbedactivity.nextBleMsg();
      //                }
      //
      //                @Override
      //                public void onNothingSelected(AdapterView<?> parentView) {
      //
      //                }
      //
      //            });

      final TextView bassCountLabel = rootView.findViewById(R.id.bassCountLabel);
      bassCountLabel.setText(
          String.format(Locale.getDefault(), "LED 1 - Bass - # of LEDs: %d", basscount));
      final SeekBar bassCountSeekBar = rootView.findViewById(R.id.bassCountSeekBar);
      bassCountSeekBar.setProgress(basscount);
      bassCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {
          // not used
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          System.out.println(TAG + "bass count changed to " + seekbar.getProgress());
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          tabbedactivity.basscount = seekbar.getProgress();
          txfifo.add(new byte[] {
              (byte) 19, (byte) ((seekbar.getProgress() >> 8) & 0xff),
              (byte) (seekbar.getProgress() & 0xff)
          });
          nextBleMsg();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
          bassCountLabel.setText(
              String.format(Locale.getDefault(), "LED 1 - Bass - # of LEDs: %d", progress));
        }
      });
      final Button oneCountMinusButton = rootView.findViewById(R.id.oneCountMinusButton);
      oneCountMinusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out1 bass count minus button pressed");
        if (bassCountSeekBar.getProgress() > 2) {
          bassCountSeekBar.setProgress(bassCountSeekBar.getProgress() - 1);
          bassCountLabel.setText(
              String.format(Locale.getDefault(), "LED 1 - Bass - # of LEDs: %d",
                  bassCountSeekBar.getProgress()));
        }
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 19, (byte) ((bassCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (bassCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      final Button oneCountPlusButton = rootView.findViewById(R.id.oneCountPlusButton);
      oneCountPlusButton.setOnClickListener(v -> {
        System.out.println(TAG + "out1 bass count plus button pressed");
        bassCountSeekBar.setProgress(bassCountSeekBar.getProgress() + 1);
        bassCountLabel.setText(
            String.format(Locale.getDefault(), "LED 1 - Bass - # of LEDs: %d",
                bassCountSeekBar.getProgress()));
        // TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        txfifo.add(new byte[] {
            (byte) 19, (byte) ((bassCountSeekBar.getProgress() >> 8) & 0xff),
            (byte) (bassCountSeekBar.getProgress() & 0xff)
        });
        nextBleMsg();
      });

      //            final Spinner bassprotocol = (Spinner)rootView.findViewById(R.id.bassProtocol);
      //            bassprotocol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      //                @Override
      //                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
      //                    System.out.println(TAG + "bass protocol selected " + position);
      //                    //TabbedActivity tabbedactivity = (TabbedActivity)getActivity();
      //                    //tabbedactivity.txfifo.add(new byte[]{(byte)( (rangenum*10) + 7), 0, (byte)position});
      //                    //tabbedactivity.nextBleMsg();
      //                }
      //
      //                @Override
      //                public void onNothingSelected(AdapterView<?> parentView) {
      //
      //                }
      //
      //            });
      return rootView;
    }
  }

  public static class RangeSettingsFragment extends Fragment {
    SeekBar cbar;
    SeekBar bbar;
    Integer brightness;
    Integer color;
    Integer options;
    Integer animation;
    Integer autochange;

    public RangeSettingsFragment() {

    }

    static RangeSettingsFragment newInstance(int sectionNumber, Context c) {
      RangeSettingsFragment fragment = new RangeSettingsFragment();
      Bundle args = new Bundle();
      args.putInt("range", sectionNumber);
      //TabbedActivity tabbedactivity = (TabbedActivity)getActivity();
      TabbedActivity tabbedactivity = (TabbedActivity) c;
      switch (sectionNumber) {
        case 1:
          args.putString("title", "LED 1 - Bass");

          System.out.println("ViviLink saved data bass --> color: "
              + tabbedactivity.basscolor + " brightness: "
              + tabbedactivity.bassbrightness + " options: "
              + tabbedactivity.bassoptions + " animation: "
              + tabbedactivity.bassanimation + " autochange: "
              + tabbedactivity.bassauto);

          args.putInt("color", tabbedactivity.basscolor);
          args.putInt("brightness", tabbedactivity.bassbrightness);
          args.putInt("options", tabbedactivity.bassoptions);
          args.putInt("animation", tabbedactivity.bassanimation);
          args.putInt("autochange", tabbedactivity.bassauto);
          break;
        case 2:
          args.putString("title", "LED 2 - Mids");

          System.out.println("ViviLink saved data mid --> color: "
              + tabbedactivity.midcolor + " brightness: "
              + tabbedactivity.midbrightness + " options: "
              + tabbedactivity.midoptions + " animation: "
              + tabbedactivity.midanimation + " autochange: "
              + tabbedactivity.midauto);

          args.putInt("color", tabbedactivity.midcolor);
          args.putInt("brightness", tabbedactivity.midbrightness);
          args.putInt("options", tabbedactivity.midoptions);
          args.putInt("animation", tabbedactivity.midanimation);
          args.putInt("autochange", tabbedactivity.midauto);
          break;
        case 3:
          args.putString("title", "LED 3 - Treble");

          System.out.println("ViviLink saved data treble --> color: "
              + tabbedactivity.treblecolor + " brightness: "
              + tabbedactivity.treblebrightness + " options: "
              + tabbedactivity.trebleoptions + " animation: "
              + tabbedactivity.trebleanimation + " autochange: "
              + tabbedactivity.trebleauto);

          args.putInt("color", tabbedactivity.treblecolor);
          args.putInt("brightness", tabbedactivity.treblebrightness);
          args.putInt("options", tabbedactivity.trebleoptions);
          args.putInt("animation", tabbedactivity.trebleanimation);
          args.putInt("autochange", tabbedactivity.trebleauto);
          break;
        default:
          break;
      }
      fragment.setArguments(args);
      return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      System.out.println(TAG + "create range fragment");
      View rootView = inflater.inflate(R.layout.fragment_rangesettings, container, false);
      TextView sectionLabel = rootView.findViewById(R.id.sectionLabel);
      sectionLabel.setText(String.format("%s Light Settings", getArguments().getString("title")));

      final int rangeNum = getArguments().getInt("range");
      brightness = getArguments().getInt("brightness");
      color = getArguments().getInt("color");
      options = getArguments().getInt("options");
      System.out.println(TAG + "options int " + options);
      animation = getArguments().getInt("animation");
      autochange = getArguments().getInt("autochange");

      System.out.println("ViviLink new range fragment --> "
          + " rangeNum: " + rangeNum + " color:" + color
          + " brightness: " + brightness + " options: "
          + options + " animation: " + animation + " autochange: "
          + autochange);

      cbar = rootView.findViewById(R.id.rangeColorBar);
      cbar.setProgress(color);
      cbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        //int realchange = 0;
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          //if(realchange == 1) {
          System.out.println(TAG + "color change stop " + seekbar.getProgress() + " " + rangeNum);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          if (rangeNum == 1) {
            tabbedactivity.basscolor = seekbar.getProgress();
            tempPreset.setBassColor(tabbedactivity.basscolor);
          } else if (rangeNum == 2) {
            tabbedactivity.midcolor = seekbar.getProgress();
            tempPreset.setMidColor(tabbedactivity.midcolor);
          } else {
            tabbedactivity.treblecolor = seekbar.getProgress();
            tempPreset.setTrebleColor(tabbedactivity.treblecolor);
          }
          color = seekbar.getProgress();
          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10) + 1), 0, (byte) seekbar.getProgress() });
          nextBleMsg();
          //realchange = 0;
          //}

          updatePrefData();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekbar, int progress, boolean fromUser) {
          System.out.println(TAG + "color changing " + seekbar.getProgress() + " " + rangeNum);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          if (rangeNum == 1) {
            tabbedactivity.basscolor = seekbar.getProgress();
            tempPreset.setBassColor(tabbedactivity.basscolor);
          } else if (rangeNum == 2) {
            tabbedactivity.midcolor = seekbar.getProgress();
            tempPreset.setMidColor(tabbedactivity.midcolor);
          } else {
            tabbedactivity.treblecolor = seekbar.getProgress();
            tempPreset.setTrebleColor(tabbedactivity.treblecolor);
          }
          color = seekbar.getProgress();
          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10) + 1), 0, (byte) seekbar.getProgress() });
          nextBleMsg();

          updatePrefData();
        }
      });

      cbar.post(() -> {
        Drawable colorScale =
            ResourcesCompat.getDrawable(getResources(), R.drawable.colorbar, null);
        Integer progressWidth = cbar.getMeasuredWidth();

        TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
        colorScale = scaleDrawable(tabbedactivity, colorScale, progressWidth);
        cbar.setProgressDrawable(colorScale);
        //cbar.setProgress(color);
      });

      bbar = rootView.findViewById(R.id.rangeBrightnessBar);
      bbar.setProgress(brightness);
      bbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          System.out.println(
              TAG + "brightness change stop " + seekbar.getProgress() + " " + rangeNum);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();

          if (rangeNum == 1) {
            tabbedactivity.bassbrightness = seekbar.getProgress();
            tempPreset.setBassBright(tabbedactivity.bassbrightness);
          } else if (rangeNum == 2) {
            tabbedactivity.midbrightness = seekbar.getProgress();
            tempPreset.setMidBright(tabbedactivity.midbrightness);
          } else {
            tabbedactivity.treblebrightness = seekbar.getProgress();
            tempPreset.setTrebleBright(tabbedactivity.treblebrightness);
          }
          brightness = seekbar.getProgress();
          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10) + 2), 0, (byte) seekbar.getProgress() });
          nextBleMsg();

          updatePrefData();
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekbar, int progress, boolean fromUser) {
          System.out.println(TAG + "brightness changing " + seekbar.getProgress() + " " + rangeNum);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();
          if (rangeNum == 1) {
            tabbedactivity.bassbrightness = seekbar.getProgress();
            tempPreset.setBassBright(tabbedactivity.bassbrightness);
          } else if (rangeNum == 2) {
            tabbedactivity.midbrightness = seekbar.getProgress();
            tempPreset.setMidBright(tabbedactivity.midbrightness);
          } else {
            tabbedactivity.treblebrightness = seekbar.getProgress();
            tempPreset.setTrebleBright(tabbedactivity.treblebrightness);
          }
          brightness = seekbar.getProgress();
          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10) + 2), 0, (byte) seekbar.getProgress() });
          nextBleMsg();

          updatePrefData();
        }
      });
      bbar.post(() -> {
        Log.i(TAG, "loading brightness slider set to " + brightness);
        //bbar.setProgress(brightness);
      });
      bbar.setOnFocusChangeListener((view, b) -> {
        Log.i(TAG, "focus- brightness slider set to " + brightness);
        //bbar.setProgress(brightness);
      });

      final Spinner rangeColorSpinner = rootView.findViewById(R.id.rangeColorSpinner);
      rangeColorSpinner.setSelection(options, false);
      rangeColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position,
            long id) {
          System.out.println(TAG + "color spinner selected " + position);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();

          if (rangeNum == 1) {
            tabbedactivity.bassoptions = position;
            tempPreset.setBassColorOptions(tabbedactivity.bassoptions);
          } else if (rangeNum == 2) {
            tabbedactivity.midoptions = position;
            tempPreset.setMidColorOptions(tabbedactivity.midoptions);
          } else {
            tabbedactivity.trebleoptions = position;
            tempPreset.setTrebleColorOptions(tabbedactivity.trebleoptions);
          }
          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10) + 7), 0, (byte) position });
          nextBleMsg();

          updatePrefData();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
          System.out.println(TAG + "color spinner nothing selected");
        }
      });

      final Spinner rangeAnimationSpinner = rootView.findViewById(R.id.rangeAnimationSpinner);
      System.out.println(TAG + "animation " + animation);
      if (animation < 0) {
        System.out.println(TAG + "animation setting out of bounds " + animation);
        animation = 0;
      }
      if (animation >= rangeAnimationSpinner.getCount()) {
        System.out.println(TAG + "animation setting out of bounds " + animation);
        animation = rangeAnimationSpinner.getCount() - 1;
      }
      //rangeAnimationSpinner.setSelection(((animation - 9) > 0 ? animation - 9 : 0), false);
      rangeAnimationSpinner.setSelection(animation, false);
      rangeAnimationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position,
            long id) {
          System.out.println(TAG + "animation spinner selected " + position);
          TabbedActivity tabbedactivity = (TabbedActivity) getActivity();

          if (rangeNum == 1) {
            tabbedactivity.bassanimation = position;
            tempPreset.setBassAnimationOptions(tabbedactivity.bassanimation);
          } else if (rangeNum == 2) {
            tabbedactivity.midanimation = position;
            tempPreset.setMidAnimationOptions(tabbedactivity.midanimation);
          } else {
            tabbedactivity.trebleanimation = position;
            tempPreset.setTrebleAnimationOptions(tabbedactivity.trebleanimation);
          }

          txfifo.add(
              new byte[] { (byte) ((rangeNum * 10)), 0, (byte) (position + 9) });
          nextBleMsg();

          updatePrefData();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
          System.out.println(TAG + "animation spinner nothing selected");
        }
      });

      final CheckBox autoAnimation = rootView.findViewById(R.id.checkBox);
      autoAnimation.setChecked(autochange != 0);
      autoAnimation.setOnCheckedChangeListener((buttonView, isChecked) -> {
        System.out.println(TAG + "auto animation checkbox " + isChecked);
        TabbedActivity tabbedactivity = (TabbedActivity) getActivity();

        if (rangeNum == 1) {
          tabbedactivity.bassauto = isChecked ? 1 : 0;
          tempPreset.setBassAutoAnimation(tabbedactivity.bassauto);
        } else if (rangeNum == 2) {
          tabbedactivity.midauto = isChecked ? 1 : 0;
          tempPreset.setMidAutoAnimation(tabbedactivity.midauto);
        } else {
          tabbedactivity.trebleauto = isChecked ? 1 : 0;
          tempPreset.setTrebleAutoAnimation(tabbedactivity.trebleauto);
        }
        txfifo.add(
            new byte[] { (byte) ((rangeNum * 10) + 8), 0, (byte) (isChecked ? 1 : 0) });
        nextBleMsg();

        updatePrefData();
      });
      return rootView;
    }
  }

  public static class BleStatusFragment extends Fragment {

    public BleStatusFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    static BleStatusFragment newInstance() {
      BleStatusFragment fragment = new BleStatusFragment();
      Bundle args = new Bundle();
      fragment.setArguments(args);

      return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      System.out.println(TAG + "create status fragment");
      View rootView = inflater.inflate(R.layout.fragment_blestatus, container, false);

      TabbedActivity tabbedactivity = (TabbedActivity) getActivity();

      TextView txtBluetoothStatus = rootView.findViewById(R.id.txtBluetoothStatus);
      txtBluetoothStatus.setText(getString(R.string.str_bluetooth_status));

      tabbedactivity.mConnectionState = rootView.findViewById(R.id.connectionState);
      tabbedactivity.mConnectionState.setText(tabbedactivity.connectionState);

      TextView addressView = rootView.findViewById(R.id.device_address);
      addressView.setText(tabbedactivity.mDeviceAddress);
      tabbedactivity.mDataField = rootView.findViewById(R.id.data_value);

      Button btnFwUpdate = rootView.findViewById(R.id.btnFwUpdate);
      btnFwUpdate.setOnClickListener(v -> {
        //TextView colorObject = (TextView) findViewById(R.id.textViewColorBass);
        System.out.println(TAG + "fwupdate button pressed");
      });

      return rootView;
    }
  }

  //class SectionsPagerAdapter extends FragmentPagerAdapter {
  //  private final Context context;
  //
  //  SectionsPagerAdapter(FragmentManager fm, Context c) {
  //    super(fm);
  //    context = c;
  //  }
  //
  //  @Override
  //  public Fragment getItem(int position) {
  //    // getItem is called to instantiate the fragment for the given page.
  //    // Return a PlaceholderFragment (defined as a static inner class below).
  //    System.out.println(TAG + "tab position selected " + position);
  //    if (position > 0 && position < 4) {
  //      System.out.println(TAG + "returning new range fragment");
  //      return RangeSettingsFragment.newInstance(position, context);
  //    } else if (position == 4) {
  //      System.out.println(TAG + "returning new setup fragment");
  //      return LedSetupFragment.newInstance(context);
  //    } else {
  //      System.out.println(TAG + "returning new status fragment");
  //      return BleStatusFragment.newInstance();
  //    }
  //  }
  //
  //  @Override public int getItemPosition(@NonNull Object object) {
  //    return POSITION_NONE;
  //  }
  //
  //  @Override
  //  public int getCount() {
  //    return 5;
  //  }
  //
  //  @Override
  //  public CharSequence getPageTitle(int position) {
  //    switch (position) {
  //      case 0:
  //        return "BLE";
  //      case 1:
  //        return "Bass";
  //      case 2:
  //        return "Mid";
  //      case 3:
  //        return "Treble";
  //      case 4:
  //        return "Setup";
  //    }
  //    return null;
  //  }
  //}

  class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    SectionsPagerAdapter(@NonNull FragmentManager fm) {
      super(fm);
    }

    @NonNull @Override public androidx.fragment.app.Fragment getItem(int position) {
      System.out.println(TAG + "tab position selected " + position);
      if (position > 0 && position < 4) {
        System.out.println(TAG + "returning new range fragment");
        return RangeSettingsFragment.newInstance(position, TabbedActivity.this);
      } else if (position == 4) {
        System.out.println(TAG + "returning new setup fragment");
        return LedSetupFragment.newInstance(TabbedActivity.this);
      } else {
        System.out.println(TAG + "returning new status fragment");
        return BleStatusFragment.newInstance();
      }
    }

    @Override public int getItemPosition(@NonNull Object object) {
      return POSITION_NONE;
    }

    @Override public void notifyDataSetChanged() {
      super.notifyDataSetChanged();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
      super.destroyItem(container, position, object);
    }

    @Override
    public int getCount() {
      return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
          return "BLE";
        case 1:
          return "Bass";
        case 2:
          return "Mid";
        case 3:
          return "Treble";
        case 4:
          return "Setup";
      }
      return null;
    }
  }

  // Our handler for received Intents. This will be called whenever an Intent
  // with an action named "custom-event-name" is broadcasted.
  private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      // Get extra data included in the Intent
      String updateValue = intent.getStringExtra("updateValue");

      if (updateValue.equalsIgnoreCase("updateValue")) {

        new Handler().postDelayed(() -> {

          basscolor = intent.getIntExtra("bassColor", 0);
          bassbrightness = intent.getIntExtra("bassBrightness", 0);
          bassoptions = intent.getIntExtra("bassOptions", 0);
          bassanimation = intent.getIntExtra("bassAnimation", 0);
          bassauto = intent.getIntExtra("bassAuto", 0);

          midcolor = intent.getIntExtra("midColor", 0);
          midbrightness = intent.getIntExtra("midBrightness", 0);
          midoptions = intent.getIntExtra("midOptions", 0);
          midanimation = intent.getIntExtra("midAnimation", 0);
          midauto = intent.getIntExtra("midAuto", 0);

          treblecolor = intent.getIntExtra("trebleColor", 0);
          treblebrightness = intent.getIntExtra("trebleBrightness", 0);
          trebleoptions = intent.getIntExtra("trebleOptions", 0);
          trebleanimation = intent.getIntExtra("trebleAnimation", 0);
          trebleauto = intent.getIntExtra("trebleAuto", 0);

          //System.out.println("ViviLink updateValue --> "
          //    + " == bass == "
          //    + basscolor + " -- "
          //    + bassbrightness + " -- "
          //    + bassoptions + " -- "
          //    + bassanimation + " -- "
          //    + bassauto
          //    + " == mid == "
          //    + midcolor + " -- "
          //    + midbrightness + " -- "
          //    + midoptions + " -- "
          //    + midanimation + " -- "
          //    + midauto
          //    + " == treble == "
          //    + treblecolor + " -- "
          //    + treblebrightness + " -- "
          //    + trebleoptions + " -- "
          //    + trebleanimation + " -- "
          //    + trebleauto);

          if (mViewPager.getAdapter() != null) {

            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.

            getSupportFragmentManager().getFragments().clear();
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mSectionsPagerAdapter);

            mViewPager.setCurrentItem(viewPagerPosition);
          }
        }, 500);
      }
    }
  };
}
