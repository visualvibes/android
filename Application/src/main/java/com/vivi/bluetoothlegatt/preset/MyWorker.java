package com.vivi.bluetoothlegatt.preset;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.vivi.bluetoothlegatt.TabbedActivity;
import com.vivi.bluetoothlegatt.model.PresetModel;

public class MyWorker extends Worker {

  public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
    super(context, workerParams);
  }

  /*
   * This method is responsible for doing the work
   * so whatever work that is needed to be performed
   * we will put it here
   *
   * For example, here I am calling the method displayNotification()
   * It will display a notification
   * So that we will understand the work is executed
   * */

  @NonNull
  @Override
  public Result doWork() {
    //passValueToDevice();

    try {
      passValueToDevice();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return Result.success();
  }

  //private void loop() throws InterruptedException {
  //  for (int i = 0; i < 50; i++) {
  //    Thread.sleep(30);
  //    System.out.println("ViviLink Working --> " + i);
  //
  //    Thread.sleep(30);
  //    System.out.println("ViviLink Working --> " + (i * 2));
  //
  //    Thread.sleep(30);
  //    System.out.println("ViviLink Working --> " + (i * 3));
  //
  //    Thread.sleep(30);
  //    System.out.println("ViviLink Working --> " + (i * 4));
  //  }
  //}

  private void passValueToDevice() throws InterruptedException {

    try {

      PresetModel presetModel = PresetListActivity.backGroundPreset;

      System.out.println("ViviLink MyWorker --> passValueToDevice");

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (11), 0, (byte) presetModel.getBassColor() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (12), 0, (byte) presetModel.getBassBright() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (17), 0, (byte) presetModel.getBassColorOptions() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (10), 0, (byte) (presetModel.getBassAnimationOptions() + 9) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (18), 0, (byte) (presetModel.getBassAutoAnimation()) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (21), 0, (byte) presetModel.getMidColor() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (22), 0, (byte) presetModel.getMidBright() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (27), 0, (byte) presetModel.getMidColorOptions() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (20), 0, (byte) (presetModel.getMidAnimationOptions() + 9) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (28), 0, (byte) (presetModel.getMidAutoAnimation()) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (31), 0, (byte) presetModel.getTrebleColor() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (32), 0, (byte) presetModel.getTrebleBright() });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] {
              (byte) (37), 0, (byte) (presetModel.getTrebleColorOptions())
          });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (30), 0, (byte) (presetModel.getTrebleAnimationOptions() + 9) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (38), 0, (byte) (presetModel.getTrebleAutoAnimation()) });
      TabbedActivity.nextBleMsg();
    } catch (Exception e) {
      System.out.println("ViviLink MyWorker error --> " + e.getLocalizedMessage());
    }
  }
}
