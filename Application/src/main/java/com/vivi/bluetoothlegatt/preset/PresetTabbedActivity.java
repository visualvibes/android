package com.vivi.bluetoothlegatt.preset;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.res.ResourcesCompat;
import androidx.legacy.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.vivi.bluetoothlegatt.BaseActivity;
import com.vivi.bluetoothlegatt.R;
import com.vivi.bluetoothlegatt.database.TablePresetMaster;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.utils.NonSwappableViewPager;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;
import java.util.Locale;

// import com.vivi.bluetoothlegatt.BluetoothLeService;
// import com.vivi.bluetoothlegatt.DeviceScanActivity;
// import com.vivi.bluetoothlegatt.SampleGattAttributes;

public class PresetTabbedActivity extends BaseActivity implements ActionBar.TabListener {

  private final static String TAG = "ViviLink PresetTabbed";

  private SectionsPagerAdapter mSectionsPagerAdapter;
  private NonSwappableViewPager mViewPager;

  private TablePresetMaster tablePresetMaster;

  private String type = "";
  private String presetName = "";
  private int presetId = 0;

  //saved values
  private int basscolor = 0; //element 4
  private int bassbrightness = 0; //7
  private int bassoptions = 0; //10
  private int bassanimation = 0; //1
  private int bassauto = 0; //11
  private int midcolor = 0; //5
  private int midbrightness = 0; //8
  private int midoptions = 0; //12
  private int midanimation = 0; //2
  private int midauto = 0; //13
  private int treblecolor = 0; //6
  private int treblebrightness = 0; //9
  private int trebleoptions = 0; //14
  private int trebleanimation = 0; //3
  private int trebleauto = 0; //15

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.i(TAG, "onCreate top");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_preset_tabbed);

    tablePresetMaster = new TablePresetMaster(getDatabaseHandler());

    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager(), this);

    // Set up the ViewPager with the sections adapter.
    mViewPager = findViewById(R.id.container);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    // Set up the action bar.
    final ActionBar actionBar = getActionBar();
    if (actionBar != null) {
      actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    }

    // When swiping between different sections, select the corresponding
    // tab. We can also use ActionBar.Tab#select() to do this if we have
    // a reference to the Tab.
    mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      @Override
      public void onPageSelected(int position) {
        Log.d(TAG, "tab change to " + position);

        RangeSettingsFragment frag;

        switch (position) {
          case 0:
            Log.i(TAG, String.format(Locale.getDefault(), "bass brightness %d", bassbrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);
            if (frag.bbar != null) {
              Log.i(TAG, "bass brightness setting restored to " + bassbrightness);
              frag.bbar.setProgress(bassbrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(basscolor);
            }
            break;
          case 1:
            Log.i(TAG, String.format(Locale.getDefault(), "mid brightness %d", midbrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);

            if (frag.bbar != null) {
              Log.i(TAG, "mid brightness setting restored to " + midbrightness);
              frag.bbar.setProgress(midbrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(midcolor);
            }
            break;
          case 2:
            Log.i(TAG,
                String.format(Locale.getDefault(), "treble brightness %d", treblebrightness));
            frag = (RangeSettingsFragment) mSectionsPagerAdapter.getItem(position);
            if (frag.bbar != null) {
              Log.i(TAG, "treble brightness setting restored to " + treblebrightness);
              frag.bbar.setProgress(treblebrightness);
            }
            if (frag.cbar != null) {
              frag.cbar.setProgress(treblecolor);
            }

            break;
          default:

            break;
        }
        if (actionBar != null) {
          actionBar.setSelectedNavigationItem(position);
        }
      }
    });

    // For each of the sections in the app, add a tab to the action bar.
    for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
      // Create a tab with text corresponding to the page title defined by
      // the adapter. Also specify this Activity object, which implements
      // the TabListener interface, as the callback (listener) for when
      // this tab is selected.
      if (actionBar != null) {
        actionBar.addTab(
            actionBar.newTab()
                .setText(mSectionsPagerAdapter.getPageTitle(i))
                .setTabListener(this));
      }
    }

    final Intent intent = getIntent();

    type = intent.getStringExtra("type");
    presetName = intent.getStringExtra("presetName");
    presetId = intent.getIntExtra("presetId", 0);

    basscolor = intent.getIntExtra("basscolor", 0);
    bassbrightness = intent.getIntExtra("bassbrightness", 0);
    bassoptions = intent.getIntExtra("bassoptions", 0);
    bassanimation = intent.getIntExtra("bassanimation", 0);
    bassauto = intent.getIntExtra("bassauto", 0);

    midcolor = intent.getIntExtra("midcolor", 0);
    midbrightness = intent.getIntExtra("midbrightness", 0);
    midoptions = intent.getIntExtra("midoptions", 0);
    midanimation = intent.getIntExtra("midanimation", 0);
    midauto = intent.getIntExtra("midauto", 0);

    treblecolor = intent.getIntExtra("treblecolor", 0);
    treblebrightness = intent.getIntExtra("treblebrightness", 0);
    trebleoptions = intent.getIntExtra("trebleoptions", 0);
    trebleanimation = intent.getIntExtra("trebleanimation", 0);
    trebleauto = intent.getIntExtra("trebleauto", 0);

    AppCompatButton btnSave = findViewById(R.id.btnSave);
    AppCompatButton btnCancel = findViewById(R.id.btnCancel);

    if (type.equalsIgnoreCase("add")) {
      if (actionBar != null) {
        actionBar.setTitle("Add New Preset");
      }
      btnSave.setText(getString(R.string.str_save));
    } else {
      if (actionBar != null) {
        actionBar.setTitle(presetName);
      }
      btnSave.setText(getString(R.string.str_update));
    }

    btnSave.setOnClickListener(v -> {
      if (type.equalsIgnoreCase("add")) {
        showPresetDialog();
      } else {
        updatePresetData();
      }
    });

    btnCancel.setOnClickListener(v -> finish());
  }

  private static Drawable scaleDrawable(Context context, Drawable image, Integer width) {
    if (width == 0) {
      return image;
    }
    if (!(image instanceof BitmapDrawable)) {
      return image;
    }
    Bitmap b = ((BitmapDrawable) image).getBitmap();
    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, width, 1, false);
    image = new BitmapDrawable(context.getResources(), bitmapResized);
    return image;
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    finish();
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d(TAG, "onResume");
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.d(TAG, "onPause()");
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
  }

  @Override
  public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    // When the given tab is selected, switch to the corresponding page in
    // the ViewPager.
    mViewPager.setCurrentItem(tab.getPosition());
  }

  @Override
  public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
  }

  @Override
  public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
  }

  public static class RangeSettingsFragment extends Fragment {
    SeekBar cbar;
    SeekBar bbar;
    Integer brightness;
    Integer color;
    Integer options;
    Integer animation;
    Integer autochange;

    public RangeSettingsFragment() {

    }

    static RangeSettingsFragment newInstance(int sectionNumber, Context c) {
      RangeSettingsFragment fragment = new RangeSettingsFragment();
      Bundle args = new Bundle();
      args.putInt("range", sectionNumber);
      //TabbedActivity tabbedactivity = (TabbedActivity)getActivity();
      PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) c;
      switch (sectionNumber) {
        case 0:
          args.putString("title", "LED 1 - Bass");
          Log.i(TAG, "packing saved data for bass -> "
              + tabbedactivity.basscolor
              + " "
              + tabbedactivity.bassbrightness
              + " "
              + tabbedactivity.bassoptions);
          args.putInt("color", tabbedactivity.basscolor);
          args.putInt("brightness", tabbedactivity.bassbrightness);
          args.putInt("options", tabbedactivity.bassoptions);
          args.putInt("animation", tabbedactivity.bassanimation);
          args.putInt("autochange", tabbedactivity.bassauto);
          break;
        case 1:
          args.putString("title", "LED 2 - Mids");
          Log.i(TAG, "packing saved data for mid -> "
              + tabbedactivity.midcolor
              + " "
              + tabbedactivity.midbrightness
              + " "
              + tabbedactivity.midoptions);
          args.putInt("color", tabbedactivity.midcolor);
          args.putInt("brightness", tabbedactivity.midbrightness);
          args.putInt("options", tabbedactivity.midoptions);
          args.putInt("animation", tabbedactivity.midanimation);
          args.putInt("autochange", tabbedactivity.midauto);
          break;
        case 2:
          args.putString("title", "LED 3 - Treble");
          Log.i(TAG, "packing saved data for treble -> "
              + sectionNumber
              + " "
              + tabbedactivity.treblecolor
              + " "
              + tabbedactivity.treblebrightness
              + " "
              + tabbedactivity.trebleoptions);
          args.putInt("color", tabbedactivity.treblecolor);
          args.putInt("brightness", tabbedactivity.treblebrightness);
          args.putInt("options", tabbedactivity.trebleoptions);
          args.putInt("animation", tabbedactivity.trebleanimation);
          args.putInt("autochange", tabbedactivity.trebleauto);
          break;
        default:
          break;
      }
      fragment.setArguments(args);
      return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      Log.d(TAG, "create range fragment");
      View rootView = inflater.inflate(R.layout.fragment_rangesettings, container, false);
      TextView sectionLabel = rootView.findViewById(R.id.sectionLabel);
      sectionLabel.setText(String.format("%s Light Settings", getArguments().getString("title")));

      final int rangeNum = getArguments().getInt("range");
      brightness = getArguments().getInt("brightness");
      color = getArguments().getInt("color");
      options = getArguments().getInt("options");
      Log.d(TAG, "options int " + options);
      animation = getArguments().getInt("animation");
      autochange = getArguments().getInt("autochange");

      Log.d(TAG, "new range fragment rangeNum "
          + rangeNum
          + " color "
          + color
          + " brightness "
          + brightness
          + " options "
          + options);
      cbar = rootView.findViewById(R.id.rangeColorBar);
      cbar.setProgress(color);
      cbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        //int realchange = 0;
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          //if(realchange == 1) {
          Log.d(TAG, "color change stop " + seekbar.getProgress() + " " + rangeNum);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.basscolor = seekbar.getProgress();
          } else if (rangeNum == 1) {
            tabbedactivity.midcolor = seekbar.getProgress();
          } else {
            tabbedactivity.treblecolor = seekbar.getProgress();
          }
          color = seekbar.getProgress();
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10) + 1), 0, (byte) seekbar.getProgress() });

        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekbar, int progress, boolean fromUser) {
          Log.d(TAG, "color changing " + seekbar.getProgress() + " " + rangeNum);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.basscolor = seekbar.getProgress();
          } else if (rangeNum == 1) {
            tabbedactivity.midcolor = seekbar.getProgress();
          } else {
            tabbedactivity.treblecolor = seekbar.getProgress();
          }
          color = seekbar.getProgress();
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10) + 1), 0, (byte) seekbar.getProgress() });
        }
      });
      cbar.post(() -> {
        Drawable colorScale =
            ResourcesCompat.getDrawable(getResources(), R.drawable.colorbar, null);
        Integer progressWidth = cbar.getMeasuredWidth();

        PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
        colorScale = scaleDrawable(tabbedactivity, colorScale, progressWidth);
        cbar.setProgressDrawable(colorScale);
        //cbar.setProgress(color);
      });
      bbar = rootView.findViewById(R.id.rangeBrightnessBar);
      bbar.setProgress(brightness);
      bbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStartTrackingTouch(SeekBar seekbar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekbar) {
          Log.d(TAG, "brightness change stop " + seekbar.getProgress() + " " + rangeNum);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.bassbrightness = seekbar.getProgress();
          } else if (rangeNum == 1) {
            tabbedactivity.midbrightness = seekbar.getProgress();
          } else {
            tabbedactivity.treblebrightness = seekbar.getProgress();
          }
          brightness = seekbar.getProgress();
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10) + 2), 0, (byte) seekbar.getProgress() });
        }

        @Override
        public void onProgressChanged(@NonNull SeekBar seekbar, int progress, boolean fromUser) {
          Log.d(TAG, "brightness changing " + seekbar.getProgress() + " " + rangeNum);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.bassbrightness = seekbar.getProgress();
          } else if (rangeNum == 1) {
            tabbedactivity.midbrightness = seekbar.getProgress();
          } else {
            tabbedactivity.treblebrightness = seekbar.getProgress();
          }
          brightness = seekbar.getProgress();
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10) + 2), 0, (byte) seekbar.getProgress() });
        }
      });
      bbar.post(() -> {
        Log.i(TAG, "loading brightness slider set to " + brightness);
        //bbar.setProgress(brightness);
      });
      bbar.setOnFocusChangeListener((view, b) -> {
        Log.i(TAG, "focus- brightness slider set to " + brightness);
        //bbar.setProgress(brightness);
      });

      final Spinner rangeColorSpinner = rootView.findViewById(R.id.rangeColorSpinner);
      rangeColorSpinner.setSelection(options, false);
      rangeColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position,
            long id) {
          Log.d(TAG, "color spinner selected " + position);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.bassoptions = position;
          } else if (rangeNum == 1) {
            tabbedactivity.midoptions = position;
          } else {
            tabbedactivity.trebleoptions = position;
          }
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10) + 7), 0, (byte) position });
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
          Log.d(TAG, "color spinner nothing selected");
        }
      });
      final Spinner rangeAnimationSpinner = rootView.findViewById(R.id.rangeAnimationSpinner);
      Log.d(TAG, "animation " + animation);
      if (animation < 0) {
        Log.d(TAG, "animation setting out of bounds " + animation);
        animation = 0;
      }
      if (animation >= rangeAnimationSpinner.getCount()) {
        Log.d(TAG, "animation setting out of bounds " + animation);
        animation = rangeAnimationSpinner.getCount() - 1;
      }
      //aspin.setSelection(((animation - 9) > 0 ? animation - 9 : 0), false);
      rangeAnimationSpinner.setSelection(animation, false);
      rangeAnimationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position,
            long id) {
          Log.d(TAG, "animation spinner selected " + position);
          PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
          if (rangeNum == 0) {
            tabbedactivity.bassanimation = position;
          } else if (rangeNum == 1) {
            tabbedactivity.midanimation = position;
          } else {
            tabbedactivity.trebleanimation = position;
          }
          //tabbedactivity.txfifo.add(
          //    new byte[] { (byte) ((rangeNum * 10)), 0, (byte) (position + 9) });
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
          Log.d(TAG, "animation spinner nothing selected");
        }
      });
      final CheckBox autoAnimation = rootView.findViewById(R.id.checkBox);
      autoAnimation.setChecked(autochange != 0);
      autoAnimation.setOnCheckedChangeListener((buttonView, isChecked) -> {
        Log.d(TAG, "auto animation checkbox " + isChecked);
        PresetTabbedActivity tabbedactivity = (PresetTabbedActivity) getActivity();
        if (rangeNum == 0) {
          tabbedactivity.bassauto = isChecked ? 1 : 0;
        } else if (rangeNum == 1) {
          tabbedactivity.midauto = isChecked ? 1 : 0;
        } else {
          tabbedactivity.trebleauto = isChecked ? 1 : 0;
        }
        //tabbedactivity.txfifo.add(
        //    new byte[] { (byte) ((rangeNum * 10) + 8), 0, (byte) (isChecked ? 1 : 0) });
      });
      return rootView;
    }
  }

  /*
   * A placeholder fragment containing a simple view.
   */
  //public static class PlaceholderFragment extends Fragment {
  //  /**
  //   * The fragment argument representing the section number for this
  //   * fragment.
  //   */
  //  private static final String ARG_SECTION_NUMBER = "section_number";
  //
  //  public PlaceholderFragment() {
  //  }
  //
  //  /**
  //   * Returns a new instance of this fragment for the given section
  //   * number.
  //   */
  //  public static PlaceholderFragment newInstance(int sectionNumber) {
  //    PlaceholderFragment fragment = new PlaceholderFragment();
  //    Bundle args = new Bundle();
  //    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
  //    fragment.setArguments(args);
  //    return fragment;
  //  }
  //
  //  @Override
  //  public View onCreateView(LayoutInflater inflater, ViewGroup container,
  //      Bundle savedInstanceState) {
  //    View rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);
  //    TextView textView = rootView.findViewById(R.id.section_label);
  //    textView.setText(
  //        getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
  //    return rootView;
  //  }
  //}

  /**
   * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
   * one of the sections/tabs/pages.
   */
  class SectionsPagerAdapter extends FragmentPagerAdapter {
    private final Context context;

    SectionsPagerAdapter(FragmentManager fm, Context c) {
      super(fm);
      context = c;
    }

    @Override
    public Fragment getItem(int position) {
      // getItem is called to instantiate the fragment for the given page.
      // Return a PlaceholderFragment (defined as a static inner class below).
      Log.d(TAG, "tab position selected " + position);
      if (position >= 0 && position < 3) {
        Log.d(TAG, "returning new range fragment");
        return RangeSettingsFragment.newInstance(position, context);
      } else {
        Log.d(TAG, "returning new range fragment");
        return RangeSettingsFragment.newInstance(0, context);
      }
    }

    @Override
    public int getCount() {
      return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
          return "Bass";
        case 1:
          return "Mid";
        case 2:
          return "Treble";
      }
      return null;
    }
  }

  private void showPresetDialog() {

    ArrayList<String> tempName = StaticDataUtility.generatePresetName();
    ArrayList<String> presetNameList = tablePresetMaster.getPresetName();

    LayoutInflater inflater = getLayoutInflater();
    @SuppressLint("InflateParams") View alertLayout =
        inflater.inflate(R.layout.dialog_preset_name, null);

    final EditText edtPresetName = alertLayout.findViewById(R.id.edtPresetName);
    final Button btnSave = alertLayout.findViewById(R.id.btnSave);
    final Button btnCancel = alertLayout.findViewById(R.id.btnCancel);

    edtPresetName.requestFocus();

    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    // this is set the view from XML inside AlertDialog
    alert.setView(alertLayout);
    // disallow cancel of AlertDialog on click of back button and outside touch
    alert.setCancelable(true);
    AlertDialog dialog = alert.create();

    for (int i = 0; i < tempName.size(); i++) {

      if (!presetNameList.contains(tempName.get(i))) {
        edtPresetName.setText(tempName.get(i));
        break;
      }
    }

    edtPresetName.setSelection(edtPresetName.getText().length());

    btnSave.setOnClickListener(v -> {

      if (edtPresetName.getText().toString().trim().equalsIgnoreCase("")) {
        showToast(getString(R.string.str_error_preset_name));
      } else if (presetNameList.contains(edtPresetName.getText().toString().trim())) {
        showToast(getString(R.string.str_error_preset_already_exist));
      } else {
        dialog.dismiss();
        saveInDatabase(edtPresetName.getText().toString().trim());
      }
    });

    btnCancel.setOnClickListener(v -> dialog.dismiss());

    dialog.show();
  }

  private void saveInDatabase(String presetName) {

    PresetModel presetModel = new PresetModel();

    presetModel.setPresetName(presetName);

    presetModel.setBassColor(basscolor);
    presetModel.setBassBright(bassbrightness);
    presetModel.setBassColorOptions(bassoptions);
    presetModel.setBassAnimationOptions(bassanimation);
    presetModel.setBassAutoAnimation(bassauto);

    presetModel.setMidColor(midcolor);
    presetModel.setMidBright(midbrightness);
    presetModel.setMidColorOptions(midoptions);
    presetModel.setMidAnimationOptions(midanimation);
    presetModel.setMidAutoAnimation(midauto);

    presetModel.setTrebleColor(treblecolor);
    presetModel.setTrebleBright(treblebrightness);
    presetModel.setTrebleColorOptions(trebleoptions);
    presetModel.setTrebleAnimationOptions(trebleanimation);
    presetModel.setTrebleAutoAnimation(trebleauto);

    tablePresetMaster.insertData(presetModel);

    showToast(getString(R.string.str_success_save_preset));

    finish();
  }

  private void updatePresetData() {

    PresetModel presetModel = new PresetModel();

    presetModel.setPresetName(presetName);
    presetModel.setPresetId(presetId);

    presetModel.setBassColor(basscolor);
    presetModel.setBassBright(bassbrightness);
    presetModel.setBassColorOptions(bassoptions);
    presetModel.setBassAnimationOptions(bassanimation);
    presetModel.setBassAutoAnimation(bassauto);

    presetModel.setMidColor(midcolor);
    presetModel.setMidBright(midbrightness);
    presetModel.setMidColorOptions(midoptions);
    presetModel.setMidAnimationOptions(midanimation);
    presetModel.setMidAutoAnimation(midauto);

    presetModel.setTrebleColor(treblecolor);
    presetModel.setTrebleBright(treblebrightness);
    presetModel.setTrebleColorOptions(trebleoptions);
    presetModel.setTrebleAnimationOptions(trebleanimation);
    presetModel.setTrebleAutoAnimation(trebleauto);

    tablePresetMaster.updatePresetData(presetModel);

    if (presetName.equalsIgnoreCase(StaticDataUtility.presetName)) {
      SharedPreferences.Editor prefsEditor = sharedpreferences.edit();
      StaticDataUtility.presetName = "";
      prefsEditor.putString("presetName", StaticDataUtility.presetName );
      prefsEditor.apply();
    }

    showToast(getString(R.string.str_success_update_preset));

    finish();
  }
}
