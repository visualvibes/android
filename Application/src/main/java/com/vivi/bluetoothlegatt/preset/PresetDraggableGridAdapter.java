/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.vivi.bluetoothlegatt.preset;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.vivi.bluetoothlegatt.R;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;

public class PresetDraggableGridAdapter
    extends RecyclerView.Adapter<PresetDraggableGridAdapter.MyViewHolder>
    implements DraggableItemAdapter<PresetDraggableGridAdapter.MyViewHolder> {

  private ArrayList<PresetModel> presetList = new ArrayList<>();
  private GridPresetClickListener presetClickListener;
  private GridUpdateListInterface updateListInterface;
  private SharedPreferences.Editor editor;

  public interface GridUpdateListInterface {
    void updateList(ArrayList<PresetModel> presetList);
  }

  public interface GridPresetClickListener {

    void onClick(int position, PresetModel presetModel);

    // void updatePresetIcon(boolean changePresetIcon);

    void onLongClick(int position, PresetModel presetModel);
  }

  PresetDraggableGridAdapter(GridPresetClickListener presetClickListener,
      GridUpdateListInterface updateListInterface,
      SharedPreferences sharedpreferences) {
    this.presetClickListener = presetClickListener;
    this.updateListInterface = updateListInterface;
    editor = sharedpreferences.edit();

    setHasStableIds(true);
  }

  class MyViewHolder extends AbstractDraggableItemViewHolder {

    TextView txtPresetName;
    ImageView imgMore;
    RelativeLayout rlRoot;

    MyViewHolder(View view) {
      super(view);
      txtPresetName = view.findViewById(R.id.txtPresetName);
      imgMore = view.findViewById(R.id.imgMore);
      rlRoot = view.findViewById(R.id.rlRoot);
    }
  }

  //public PresetDraggableGridAdapter(AbstractDataProvider dataProvider) {
  //  mProvider = dataProvider;
  //
  //  // DraggableItemAdapter requires stable ID, and also
  //  // have to implement the getItemId() method appropriately.
  //  setHasStableIds(true);
  //}

  //public void setItemMoveMode(int itemMoveMode) {
  //  mItemMoveMode = itemMoveMode;
  //}
  //
  //@Override
  //public long getItemId(int position) {
  //  return mProvider.getItem(position).getId();
  //}
  //
  //@Override
  //public int getItemViewType(int position) {
  //  return mProvider.getItem(position).getViewType();
  //}

  @NonNull
  @Override
  public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.preset_list_item, parent, false);

    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    PresetModel presetModel = presetList.get(position);
    holder.txtPresetName.setText(presetModel.getPresetName());

    holder.imgMore.setTag(position);
    holder.imgMore.setOnClickListener(v -> {

      int pos = (int) v.getTag();
      if (presetClickListener != null) {
        presetClickListener.onLongClick(pos, presetList.get(pos));
      }
    });

    holder.rlRoot.setTag(position);
    holder.rlRoot.setOnClickListener(v -> {

      int pos = (int) v.getTag();

      if (!presetList.get(pos).isPresetOn()) {
        updateSelection(pos);
      }

      if (presetClickListener != null) {
        presetClickListener.onClick(pos, presetList.get(pos));
      }
    });

    if (presetModel.isPresetOn()) {
      holder.rlRoot.setSelected(true);
    } else {
      holder.rlRoot.setSelected(false);
    }
  }

  @Override
  public int getItemCount() {
    return presetList.size();
  }

  @Override
  public long getItemId(int position) {
    return presetList.get(position).getId();
  }

  //@Override
  //public long getItemId(int position) {
  //  return position;
  //}

  @Override
  public void onMoveItem(int fromPosition, int toPosition) {

    if (fromPosition == toPosition) {
      return;
    }

    final PresetModel item = presetList.remove(fromPosition);
    presetList.add(toPosition, item);
  }

  @Override
  public boolean onCheckCanStartDrag(@NonNull MyViewHolder holder, int position, int x, int y) {
    return true;
  }

  @Override
  public ItemDraggableRange onGetItemDraggableRange(@NonNull MyViewHolder holder, int position) {
    // no drag-sortable range specified
    return null;
  }

  @Override
  public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
    return true;
  }

  @Override
  public void onItemDragStarted(int position) {
    // notifyDataSetChanged();
  }

  @Override
  public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {

    if (fromPosition != toPosition) {

      notifyDataSetChanged();

      if (updateListInterface != null) {
        updateListInterface.updateList(presetList);
      }
    }
  }

  void addList(ArrayList<PresetModel> presetList) {
    //this.isPresetOn = isPresetOn;
    this.presetList.clear();
    this.presetList.addAll(presetList);
    notifyDataSetChanged();
  }

  void updateList(ArrayList<PresetModel> presetList) {
    //this.isPresetOn = isPresetOn;
    this.presetList.clear();
    this.presetList.addAll(presetList);
  }

  ArrayList<PresetModel> getPresetList() {
    return presetList;
  }

  void updateSelectionOFF() {

    for (int i = 0; i < presetList.size(); i++) {
      PresetModel presetModel = presetList.get(i);
      presetModel.setPresetOn(false);
      presetList.set(i, presetModel);
    }

    StaticDataUtility.presetName = "";
    editor.putString("presetName", StaticDataUtility.presetName);
    editor.apply();

    notifyDataSetChanged();
  }

  private void updateSelection(int position) {

    StaticDataUtility.presetName = "";
    editor.putString("presetName", StaticDataUtility.presetName);
    editor.apply();

    for (int i = 0; i < presetList.size(); i++) {

      PresetModel presetModel = presetList.get(i);

      if (i == position) {

        if (!presetModel.isPresetOn()) {
          StaticDataUtility.presetName = presetModel.getPresetName();
          editor.putString("presetName", StaticDataUtility.presetName);
          editor.apply();
        }

        presetModel.setPresetOn(!presetModel.isPresetOn());
      } else {
        presetModel.setPresetOn(false);
      }

      presetList.set(i, presetModel);
    }
    notifyDataSetChanged();
  }
}