package com.vivi.bluetoothlegatt.preset;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import com.google.gson.Gson;
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.vivi.bluetoothlegatt.BaseActivity;
import com.vivi.bluetoothlegatt.R;
import com.vivi.bluetoothlegatt.TabbedActivity;
import com.vivi.bluetoothlegatt.database.TablePresetMaster;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;

public class PresetListActivity extends BaseActivity implements View.OnClickListener,
    PresetDraggableGridAdapter.GridUpdateListInterface,
    PresetDraggableGridAdapter.GridPresetClickListener {

  private final static String TAG = "ViviLink PresetList";

  private TablePresetMaster tablePresetMaster;

  public static PresetModel backGroundPreset = new PresetModel();
  private SharedPreferences.Editor prefsEditor;

  private RecyclerView recyclerViewPreset;
  private RecyclerView.LayoutManager mLayoutManager;
  private PresetDraggableGridAdapter mAdapter;
  private RecyclerView.Adapter mWrappedAdapter;
  private RecyclerViewDragDropManager mRecyclerViewDragDropManager;

  private AppCompatImageView presetToggleOn, presetToggleOff;
  // private LinearLayout llPresetOnOff;
  private boolean isPresetOn = false;

  private PresetModel presetModel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.i(TAG, "onCreate top");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_preset);

    tablePresetMaster = new TablePresetMaster(getDatabaseHandler());
    prefsEditor = sharedpreferences.edit();

    AppCompatImageView imgAddPreset = findViewById(R.id.imgAddPreset);
    imgAddPreset.setOnClickListener(
        v -> addPreset("add"));

    setPresetOnOffView();
    getIntentData();
    setRecyclerView();
  }

  //private void getIntentData() {
  //
  //  if (getIntent().getIntExtra("bassanimation", 0) == 20 &&
  //      getIntent().getIntExtra("midanimation", 0) == 20 &&
  //      getIntent().getIntExtra("trebleanimation", 0) == 20 &&
  //      getIntent().getIntExtra("bassauto", 0) == 0 &&
  //      getIntent().getIntExtra("midauto", 0) == 0 &&
  //      getIntent().getIntExtra("trebleauto", 0) == 0
  //  ) {
  //    updatePresetIcon("off");
  //  } else {
  //    updatePresetIcon("on");
  //  }
  //
  //  if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
  //      .equalsIgnoreCase("yes")) {
  //    StaticDataUtility.presetName = "";
  //    prefsEditor.putString("presetName", StaticDataUtility.presetName);
  //    prefsEditor.apply();
  //  }
  //
  //  PresetModel intentPresetModel = new PresetModel();
  //  intentPresetModel.setBassColor(getIntent().getIntExtra("basscolor", 0));
  //  intentPresetModel.setBassBright(getIntent().getIntExtra("bassbrightness", 0));
  //  intentPresetModel.setBassColorOptions(getIntent().getIntExtra("bassoptions", 0));
  //  intentPresetModel.setBassAnimationOptions(getIntent().getIntExtra("bassanimation", 0));
  //  intentPresetModel.setBassAutoAnimation(getIntent().getIntExtra("bassauto", 0));
  //
  //  intentPresetModel.setMidColor(getIntent().getIntExtra("midcolor", 0));
  //  intentPresetModel.setMidBright(getIntent().getIntExtra("midbrightness", 0));
  //  intentPresetModel.setMidColorOptions(getIntent().getIntExtra("midoptions", 0));
  //  intentPresetModel.setMidAnimationOptions(getIntent().getIntExtra("midanimation", 0));
  //  intentPresetModel.setMidAutoAnimation(getIntent().getIntExtra("midauto", 0));
  //
  //  intentPresetModel.setTrebleColor(getIntent().getIntExtra("treblecolor", 0));
  //  intentPresetModel.setTrebleBright(getIntent().getIntExtra("treblebrightness", 0));
  //  intentPresetModel.setTrebleColorOptions(getIntent().getIntExtra("trebleoptions", 0));
  //  intentPresetModel.setTrebleAnimationOptions(getIntent().getIntExtra("trebleanimation", 0));
  //  intentPresetModel.setTrebleAutoAnimation(getIntent().getIntExtra("trebleauto", 0));
  //
  //  if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
  //      .equalsIgnoreCase("yes")) {
  //
  //    Gson gson = new Gson();
  //    prefsEditor.putString(StaticDataUtility.CURRENT_PRESET_MODEL, gson.toJson(intentPresetModel));
  //    prefsEditor.apply();
  //  }
  //}

  private void getIntentData() {

    if (getIntent().getIntExtra("bassanimation", 0) == 20 &&
        getIntent().getIntExtra("midanimation", 0) == 20 &&
        getIntent().getIntExtra("trebleanimation", 0) == 20 &&
        getIntent().getIntExtra("bassauto", 0) == 0 &&
        getIntent().getIntExtra("midauto", 0) == 0 &&
        getIntent().getIntExtra("trebleauto", 0) == 0
    ) {
      updatePresetIcon("off");
    } else {
      updatePresetIcon("on");
    }

    if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
        .equalsIgnoreCase("yes")) {
      StaticDataUtility.presetName = "";
      prefsEditor.putString("presetName", StaticDataUtility.presetName);
      prefsEditor.apply();
    }

    //PresetModel intentPresetModel = new PresetModel();
    //intentPresetModel.setBassColor(getIntent().getIntExtra("basscolor", 0));
    //intentPresetModel.setBassBright(getIntent().getIntExtra("bassbrightness", 0));
    //intentPresetModel.setBassColorOptions(getIntent().getIntExtra("bassoptions", 0));
    //intentPresetModel.setBassAutoAnimation(getIntent().getIntExtra("bassauto", 0));
    //
    //intentPresetModel.setMidColor(getIntent().getIntExtra("midcolor", 0));
    //intentPresetModel.setMidBright(getIntent().getIntExtra("midbrightness", 0));
    //intentPresetModel.setMidColorOptions(getIntent().getIntExtra("midoptions", 0));
    //intentPresetModel.setMidAutoAnimation(getIntent().getIntExtra("midauto", 0));
    //
    //intentPresetModel.setTrebleColor(getIntent().getIntExtra("treblecolor", 0));
    //intentPresetModel.setTrebleBright(getIntent().getIntExtra("treblebrightness", 0));
    //intentPresetModel.setTrebleColorOptions(getIntent().getIntExtra("trebleoptions", 0));
    //intentPresetModel.setTrebleAutoAnimation(getIntent().getIntExtra("trebleauto", 0));
    //
    //Gson gson = new Gson();
    //String json = sharedpreferences.getString(StaticDataUtility.CURRENT_PRESET_MODEL,
    //    "");
    //
    //PresetModel tempPreset = new PresetModel();
    //
    //if (!json.equalsIgnoreCase("")) {
    //  tempPreset = gson.fromJson(json, PresetModel.class);
    //}
    //
    //if (getIntent().getIntExtra("bassanimation", 0) == 20) {
    //
    //  if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
    //      .equalsIgnoreCase("yes")) {
    //    intentPresetModel.setBassAnimationOptions(getIntent().getIntExtra("bassanimation", 0));
    //  } else {
    //    if (tempPreset.getBassAnimationOptions() == 20) {
    //      intentPresetModel.setBassAnimationOptions(20);
    //    } else if (tempPreset.getBassAnimationOptions() == 0) {
    //      intentPresetModel.setBassAnimationOptions(getIntent().getIntExtra("bassanimation", 0));
    //    } else {
    //      intentPresetModel.setBassAnimationOptions(tempPreset.getBassAnimationOptions());
    //    }
    //  }
    //} else {
    //  intentPresetModel.setBassAnimationOptions(getIntent().getIntExtra("bassanimation", 0));
    //}
    //
    //if (getIntent().getIntExtra("midanimation", 0) == 20) {
    //
    //  if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
    //      .equalsIgnoreCase("yes")) {
    //    intentPresetModel.setMidAnimationOptions(getIntent().getIntExtra("midanimation", 0));
    //  } else {
    //    if (tempPreset.getMidAnimationOptions() == 20) {
    //      intentPresetModel.setMidAnimationOptions(20);
    //    } else if (tempPreset.getMidAnimationOptions() == 0) {
    //      intentPresetModel.setMidAnimationOptions(getIntent().getIntExtra("midanimation", 0));
    //    } else {
    //      intentPresetModel.setMidAnimationOptions(tempPreset.getMidAnimationOptions());
    //    }
    //  }
    //} else {
    //  intentPresetModel.setMidAnimationOptions(getIntent().getIntExtra("midanimation", 0));
    //}
    //
    //if (getIntent().getIntExtra("trebleanimation", 0) == 20) {
    //
    //  if (sharedpreferences.getString(StaticDataUtility.VALUE_CHANGE, "no")
    //      .equalsIgnoreCase("yes")) {
    //    intentPresetModel.setTrebleAnimationOptions(getIntent().getIntExtra("trebleanimation", 0));
    //  } else {
    //    if (tempPreset.getTrebleAnimationOptions() == 20) {
    //      intentPresetModel.setTrebleAnimationOptions(20);
    //    } else if (tempPreset.getTrebleAnimationOptions() == 0) {
    //      intentPresetModel.setTrebleAnimationOptions(
    //          getIntent().getIntExtra("trebleanimation", 0));
    //    } else {
    //      intentPresetModel.setTrebleAnimationOptions(tempPreset.getTrebleAnimationOptions());
    //    }
    //  }
    //} else {
    //  intentPresetModel.setTrebleAnimationOptions(getIntent().getIntExtra("trebleanimation", 0));
    //}
    //
    //prefsEditor.putString(StaticDataUtility.CURRENT_PRESET_MODEL, gson.toJson(intentPresetModel));
    //prefsEditor.apply();
  }

  private void setPresetOnOffView() {

    //llPresetOnOff = findViewById(R.id.llPresetOnOff);

    TextView txtPresetOn = findViewById(R.id.txtPresetOn);
    TextView txtPresetOff = findViewById(R.id.txtPresetOff);

    presetToggleOn = findViewById(R.id.presetToggleOn);
    presetToggleOff = findViewById(R.id.presetToggleOff);

    txtPresetOn.setOnClickListener(this);
    txtPresetOff.setOnClickListener(this);
    presetToggleOn.setOnClickListener(this);
    presetToggleOff.setOnClickListener(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    getPresetData("add");
  }

  @Override
  public void onClick(View view) {

    switch (view.getId()) {
      case R.id.txtPresetOn:
      case R.id.presetToggleOn:

        if (!isPresetOn) {

          presetToggleOn.setImageResource(R.drawable.ic_preset_on);
          presetToggleOff.setImageResource(R.drawable.ic_preset_blank);

          isPresetOn = true;

          //prefsEditor.putString(StaticDataUtility.ON_OFF_BUTTON, "on");
          //prefsEditor.apply();

          Gson gson = new Gson();
          String json = sharedpreferences.getString(StaticDataUtility.CURRENT_PRESET_MODEL,
              "");

          if (!json.equalsIgnoreCase("")) {
            backGroundPreset = gson.fromJson(json, PresetModel.class);
            passONValueToDevice();
            passIntentData(backGroundPreset);
          }
        }

        break;

      case R.id.txtPresetOff:
      case R.id.presetToggleOff:

        if (isPresetOn) {

          presetToggleOff.setImageResource(R.drawable.ic_preset_off);
          presetToggleOn.setImageResource(R.drawable.ic_preset_blank);

          isPresetOn = false;
          passOFFValueToDevice();

          Gson gson = new Gson();
          String json = sharedpreferences.getString(StaticDataUtility.CURRENT_PRESET_MODEL,
              "");
          if (!json.equalsIgnoreCase("")) {
            PresetModel newPreset = gson.fromJson(json, PresetModel.class);
            newPreset.setBassAnimationOptions(20);
            newPreset.setMidAnimationOptions(20);
            newPreset.setTrebleAnimationOptions(20);
            newPreset.setBassAutoAnimation(0);
            newPreset.setMidAutoAnimation(0);
            newPreset.setTrebleAutoAnimation(0);
            passIntentData(newPreset);
          }

          mAdapter.updateSelectionOFF();
        }

        break;
    }
  }

  //@Override
  //protected void onResume() {
  //  super.onResume();
  //  Log.d(TAG, "onResume");
  //  registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
  //  if (mBluetoothLeService != null) {
  //    final boolean result = mBluetoothLeService.connect(mDeviceAddress);
  //    Log.d(TAG, "Connect request result=" + result);
  //  }
  //}
  //
  //@Override
  //protected void onPause() {
  //  super.onPause();
  //  Log.d(TAG, "onPause()");
  //  unregisterReceiver(mGattUpdateReceiver);
  //}
  //
  //@Override
  //protected void onDestroy() {
  //  super.onDestroy();
  //  Log.d(TAG, "onDestroy");
  //  unbindService(mServiceConnection);
  //  mBluetoothLeService = null;
  //}

  @Override
  public void onBackPressed() {
    destroyData();
    super.onBackPressed();

    prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "no");
    prefsEditor.apply();
    backGroundPreset = null;

    try {
      WorkManager.getInstance().cancelAllWork();
    } catch (Exception e) {
      System.out.println("ViviLink cancelAllWork --> " + e.getLocalizedMessage());
    }

    //isPresetOn = false;
    //handler.removeCallbacksAndMessages(null);

    finish();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    try {
      WorkManager.getInstance().cancelAllWork();
    } catch (Exception e) {
      System.out.println("ViviLink cancelAllWork --> " + e.getLocalizedMessage());
    }

    //isPresetOn = false;
    //handler.removeCallbacksAndMessages(null);

    finish();
  }

  private void updatePresetIcon(String type) {

    if (type.equalsIgnoreCase("on")) {
      presetToggleOn.setImageResource(R.drawable.ic_preset_on);
      presetToggleOff.setImageResource(R.drawable.ic_preset_blank);
      isPresetOn = true;
    } else {
      presetToggleOff.setImageResource(R.drawable.ic_preset_off);
      presetToggleOn.setImageResource(R.drawable.ic_preset_blank);
      isPresetOn = false;
    }
  }

  private void setRecyclerView() {

    recyclerViewPreset = findViewById(R.id.recyclerViewPreset);
    mLayoutManager = new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);

    // drag & drop manager
    mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
    // Start dragging after long press
    mRecyclerViewDragDropManager.setInitiateOnLongPress(true);
    mRecyclerViewDragDropManager.setInitiateOnMove(false);
    mRecyclerViewDragDropManager.setLongPressTimeout(450);
    mRecyclerViewDragDropManager.setDragStartItemAnimationDuration(250);
    mRecyclerViewDragDropManager.setDraggingItemAlpha(0.8f);
    mRecyclerViewDragDropManager.setDraggingItemScale(1.3f);

    mAdapter = new PresetDraggableGridAdapter(this, this, sharedpreferences);
    mAdapter.setHasStableIds(true);

    mWrappedAdapter =
        mRecyclerViewDragDropManager.createWrappedAdapter(mAdapter);

    GeneralItemAnimator animator =
        new DraggableItemAnimator(); // DraggableItemAnimator is required to make item animations properly.

    recyclerViewPreset.setLayoutManager(mLayoutManager);
    recyclerViewPreset.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
    recyclerViewPreset.setItemAnimator(animator);

    mRecyclerViewDragDropManager.attachRecyclerView(recyclerViewPreset);
  }

  private void getPresetData(String type) {
    ArrayList<PresetModel> presetList =
        tablePresetMaster.getAllPresetData(sharedpreferences);

    //if (presetList.size() == 0) {
    //  llPresetOnOff.setEnabled(false);
    //} else {
    //  llPresetOnOff.setEnabled(true);
    //}

    if (type.equalsIgnoreCase("add")) {
      mAdapter.addList(presetList);
    } else {
      mAdapter.updateList(presetList);
    }
  }

  private void addPreset(String type) {

    final Intent nextIntent = new Intent(this, PresetTabbedActivity.class);

    nextIntent.putExtra("type", type);
    nextIntent.putExtra("presetId", type.equalsIgnoreCase("add") ? 0 : presetModel.getPresetId());
    nextIntent.putExtra("presetName",
        type.equalsIgnoreCase("add") ? "" : presetModel.getPresetName());

    nextIntent.putExtra("basscolor",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("basscolor", 0)
            : presetModel.getBassColor());
    nextIntent.putExtra("bassbrightness",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("bassbrightness", 0)
            : presetModel.getBassBright());
    nextIntent.putExtra("bassoptions",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("bassoptions", 0)
            : presetModel.getBassColorOptions());
    nextIntent.putExtra("bassanimation",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("bassanimation", 0)
            : presetModel.getBassAnimationOptions());
    nextIntent.putExtra("bassauto",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("bassauto", 0)
            : presetModel.getBassAutoAnimation());

    nextIntent.putExtra("midcolor",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("midcolor", 0)
            : presetModel.getMidColor());
    nextIntent.putExtra("midbrightness",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("midbrightness", 0)
            : presetModel.getMidBright());
    nextIntent.putExtra("midoptions",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("midoptions", 0)
            : presetModel.getMidColorOptions());
    nextIntent.putExtra("midanimation",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("midanimation", 0)
            : presetModel.getMidAnimationOptions());
    nextIntent.putExtra("midauto",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("midauto", 0)
            : presetModel.getMidAutoAnimation());

    nextIntent.putExtra("treblecolor",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("treblecolor", 0)
            : presetModel.getTrebleColor());
    nextIntent.putExtra("treblebrightness",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("treblebrightness", 0)
            : presetModel.getTrebleBright());
    nextIntent.putExtra("trebleoptions",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("trebleoptions", 0)
            : presetModel.getTrebleColorOptions());
    nextIntent.putExtra("trebleanimation",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("trebleanimation", 0)
            : presetModel.getTrebleAnimationOptions());
    nextIntent.putExtra("trebleauto",
        type.equalsIgnoreCase("add") ? getIntent().getIntExtra("trebleauto", 0)
            : presetModel.getTrebleAutoAnimation());

    startActivity(nextIntent);
  }

  @Override
  public void onClick(int position, PresetModel presetModel) {
    System.out.println("ViviLink preset onclick --> " + presetModel.getPresetName());
    passValueToDevice();
  }

  //@Override
  //public void updatePresetIcon(boolean changePresetIcon) {
  //
  //  if (changePresetIcon) {
  //
  //    //txtPresetOn.setTextColor(getResources().getColor(R.color.colorGreen));
  //    presetToggleOn.setImageResource(R.drawable.ic_preset_on);
  //
  //    //txtPresetOff.setTextColor(Color.WHITE);
  //    presetToggleOff.setImageResource(R.drawable.ic_preset_blank);
  //
  //    isPresetOn = true;
  //  } else {
  //
  //    //txtPresetOff.setTextColor(getResources().getColor(R.color.colorGreen));
  //    presetToggleOff.setImageResource(R.drawable.ic_preset_off);
  //
  //    //txtPresetOn.setTextColor(Color.WHITE);
  //    presetToggleOn.setImageResource(R.drawable.ic_preset_blank);
  //
  //    isPresetOn = false;
  //  }
  //}

  @Override
  public void onLongClick(int position, PresetModel presetModel) {
    longPressOption(position, presetModel);
  }

  private void longPressOption(int position, PresetModel presetModel) {

    this.presetModel = presetModel;

    Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_preset_option);
    dialog.setCancelable(true);

    WindowManager.LayoutParams windowManager = new WindowManager.LayoutParams();

    if (dialog.getWindow() != null) {
      windowManager.copyFrom(dialog.getWindow().getAttributes());
      windowManager.width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
      windowManager.height = WindowManager.LayoutParams.WRAP_CONTENT;
      dialog.getWindow().setLayout(windowManager.width, windowManager.height);
    }

    TextView txtPresetName = dialog.findViewById(R.id.txtPresetName);
    txtPresetName.setText(presetModel.getPresetName());

    TextView txtEditPreset = dialog.findViewById(R.id.txtEditPreset);
    TextView txtCopyPreset = dialog.findViewById(R.id.txtCopyPreset);
    TextView txtDeletePreset = dialog.findViewById(R.id.txtDeletePreset);
    TextView txtRenamePreset = dialog.findViewById(R.id.txtRenamePreset);

    txtEditPreset.setOnClickListener(
        v -> {
          dialog.dismiss();
          addPreset("update");
        });

    txtCopyPreset.setOnClickListener(v -> {

      PresetModel presetModel1 = mAdapter.getPresetList().get(position);

      ArrayList<String> tempName = StaticDataUtility.generateCopyPresetName(
          mAdapter.getPresetList().get(position).getPresetName());
      ArrayList<String> presetNameList = tablePresetMaster.getPresetName();

      for (int i = 0; i < tempName.size(); i++) {

        if (!presetNameList.contains(tempName.get(i))) {
          presetModel1.setPresetName(tempName.get(i));
          break;
        }
      }

      //if (isPresetOn) {
      //  presetModel1.setPresetOn(true);
      //} else {
      //  presetModel1.setPresetOn(false);
      //}

      tablePresetMaster.insertData(presetModel1);

      dialog.dismiss();
      getPresetData("add");
      showToast(getString(R.string.str_success_copy_preset));
    });

    txtDeletePreset.setOnClickListener(v -> {

      if (presetModel.isPresetOn()) {
        showToast("You can't delete preset as it currently selected.");
      } else {
        dialog.dismiss();
        showDeletePreset();
      }
    });

    txtRenamePreset.setOnClickListener(v -> {

      dialog.dismiss();
      showRenamePresetDialog();
    });

    dialog.show();
  }

  private void showDeletePreset() {

    AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
        //set message, title, and icon
        .setTitle("Delete Preset")
        .setMessage("Are you sure want to Delete " + presetModel.getPresetName() + "?")
        .setPositiveButton("No", (dialog, whichButton) -> dialog.dismiss())

        .setNegativeButton("Yes", (dialog, which) -> {

          boolean isDelete = tablePresetMaster.deletePreset(presetModel.getPresetId());

          if (isDelete) {

            //if (presetModel.isPresetOn()) {
            //  StaticDataUtility.presetName = presetModel.getPresetName();
            //  prefsEditor.putString("presetName", StaticDataUtility.presetName);
            //  prefsEditor.apply();
            //}

            getPresetData("add");
            showToast(getString(R.string.str_success_delete_preset));
          } else {
            showToast(getString(R.string.str_error_delete_preset));
          }
        })
        .create();

    myQuittingDialogBox.show();
  }

  private void showRenamePresetDialog() {

    LayoutInflater inflater = getLayoutInflater();
    @SuppressLint("InflateParams") View alertLayout =
        inflater.inflate(R.layout.dialog_preset_name, null);

    final TextView txtTitle = alertLayout.findViewById(R.id.txtTitle);
    final EditText edtPresetName = alertLayout.findViewById(R.id.edtPresetName);
    final Button btnSave = alertLayout.findViewById(R.id.btnSave);
    final Button btnCancel = alertLayout.findViewById(R.id.btnCancel);

    txtTitle.setText(getString(R.string.str_rename_preset));

    edtPresetName.setText(presetModel.getPresetName());
    edtPresetName.requestFocus();
    edtPresetName.setSelection(edtPresetName.getText().length());

    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    // this is set the view from XML inside AlertDialog
    alert.setView(alertLayout);
    // disallow cancel of AlertDialog on click of back button and outside touch
    alert.setCancelable(true);
    AlertDialog dialog = alert.create();

    btnSave.setText(getString(R.string.str_update));
    btnSave.setOnClickListener(v -> {

      if (edtPresetName.getText().toString().trim().equalsIgnoreCase("")) {
        showToast(getString(R.string.str_error_preset_name));
      } else if (tablePresetMaster.checkPresetNameExist(
          edtPresetName.getText().toString().trim())) {
        showToast(getString(R.string.str_error_preset_already_exist));
      } else {

        boolean isUpdate = tablePresetMaster.updatePreset(presetModel.getPresetId(),
            edtPresetName.getText().toString().trim());

        if (isUpdate) {

          if (presetModel.isPresetOn()) {
            StaticDataUtility.presetName = edtPresetName.getText().toString().trim();
            prefsEditor.putString("presetName", StaticDataUtility.presetName);
            prefsEditor.apply();
          }

          dialog.dismiss();
          getPresetData("add");
          showToast(getString(R.string.str_success_rename_preset));
        } else {
          showToast(getString(R.string.str_error_rename_preset));
        }
      }
    });

    btnCancel.setOnClickListener(v -> dialog.dismiss());

    dialog.show();
  }

  @Override
  public void updateList(ArrayList<PresetModel> presetList) {
    tablePresetMaster.insertAllData(presetList);
    getPresetData("update");
  }

  private void destroyData() {

    if (mRecyclerViewDragDropManager != null) {
      mRecyclerViewDragDropManager.release();
      mRecyclerViewDragDropManager = null;
    }

    if (recyclerViewPreset != null) {
      recyclerViewPreset.setItemAnimator(null);
      recyclerViewPreset.setAdapter(null);
      recyclerViewPreset = null;
    }

    if (mWrappedAdapter != null) {
      WrapperAdapterUtils.releaseAll(mWrappedAdapter);
      mWrappedAdapter = null;
    }
    mAdapter = null;
    mLayoutManager = null;
  }

  private void passValueToDevice() {

    try {

      if (!StaticDataUtility.presetName.equalsIgnoreCase("")) {

        ArrayList<PresetModel> tempList = mAdapter.getPresetList();

        for (int i = 0; i < tempList.size(); i++) {

          if (tempList.get(i)
              .getPresetName()
              .equalsIgnoreCase(StaticDataUtility.presetName)) {
            backGroundPreset = tempList.get(i);
          }
        }

        Gson gson = new Gson();
        String json = gson.toJson(backGroundPreset);
        prefsEditor.putString(StaticDataUtility.CURRENT_PRESET_MODEL, json);
        prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "no");
        prefsEditor.apply();

        OneTimeWorkRequest oneTimeWorkRequest =
            new OneTimeWorkRequest.Builder(MyWorker.class).build();
        WorkManager.getInstance().enqueue(oneTimeWorkRequest);

        passIntentData(backGroundPreset);

        if (backGroundPreset.getBassAnimationOptions() == 20 &&
            backGroundPreset.getMidAnimationOptions() == 20 &&
            backGroundPreset.getTrebleAnimationOptions() == 20 &&
            backGroundPreset.getBassAutoAnimation() == 0 &&
            backGroundPreset.getMidAutoAnimation() == 0 &&
            backGroundPreset.getTrebleAutoAnimation() == 0
        ) {
          updatePresetIcon("off");
        } else {
          updatePresetIcon("on");
        }

        // updatePresetIcon("on");
      }
    } catch (Exception e) {
      System.out.println("ViviLink passValueToDevice error --> " + e.getLocalizedMessage());
    }
  }

  private void passIntentData(PresetModel newPresetModel) {

    Intent intent = new Intent("updateValue");

    intent.putExtra("updateValue", "updateValue");

    intent.putExtra("bassColor", newPresetModel.getBassColor());
    intent.putExtra("bassBrightness", newPresetModel.getBassBright());
    intent.putExtra("bassOptions", newPresetModel.getBassColorOptions());
    intent.putExtra("bassAnimation", newPresetModel.getBassAnimationOptions());
    intent.putExtra("bassAuto", newPresetModel.getBassAutoAnimation());

    intent.putExtra("midColor", newPresetModel.getMidColor());
    intent.putExtra("midBrightness", newPresetModel.getMidBright());
    intent.putExtra("midOptions", newPresetModel.getMidColorOptions());
    intent.putExtra("midAnimation", newPresetModel.getMidAnimationOptions());
    intent.putExtra("midAuto", newPresetModel.getMidAutoAnimation());

    intent.putExtra("trebleColor", newPresetModel.getTrebleColor());
    intent.putExtra("trebleBrightness", newPresetModel.getTrebleBright());
    intent.putExtra("trebleOptions", newPresetModel.getTrebleColorOptions());
    intent.putExtra("trebleAnimation", newPresetModel.getTrebleAnimationOptions());
    intent.putExtra("trebleAuto", newPresetModel.getTrebleAutoAnimation());

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  private void passONValueToDevice() {

    prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "no");
    prefsEditor.apply();

    OneTimeWorkRequest oneTimeWorkRequest =
        new OneTimeWorkRequest.Builder(MyWorker.class).build();
    WorkManager.getInstance().enqueue(oneTimeWorkRequest);
  }

  private void passOFFValueToDevice() {

    prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "no");
    prefsEditor.apply();

    try {

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (10), 0, (byte) (29) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (20), 0, (byte) (29) });
      TabbedActivity.nextBleMsg();

      Thread.sleep(70);

      TabbedActivity.txfifo.add(
          new byte[] { (byte) (30), 0, (byte) (29) });
      TabbedActivity.nextBleMsg();
    } catch (Exception e) {
      System.out.println("ViviLink passOFFValueToDevice error --> " + e.getLocalizedMessage());
    }
  }
}