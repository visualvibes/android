/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vivi.bluetoothlegatt;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
  static final String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
  static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
  public static final String UART_TX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
  public static final String UART_RX = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
  public static final String UART_FLOW = "6e400006-b5a3-f393-e0a9-e50e24dcca9e";
  public static final String UART_ENABLE = "6e400008-b5a3-f393-e0a9-e50e24dcca9e";
  public static final String CTRL_POINT = "2413b43f-707f-90bd-2045-2ab8807571b7";
  public static final String UART_BAUD = "6e400004-b5a3-f393-e0a9-e50e24dcca9e";
  private static final HashMap<String, String> attributes = new HashMap<>();

  static {
    // Sample Services.
    attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
    attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
    // Sample Characteristics.
    attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
    attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
    attributes.put("2413b33f-707f-90bd-2045-2ab8807571b7", "Rigado Control Service");
    attributes.put("6e400001-b5a3-f393-e0a9-e50e24dcca9e", "Rigado UART Service");
    attributes.put(UART_TX, "UART TX");
    attributes.put(UART_RX, "UART RX");
    attributes.put(UART_ENABLE, "UART Enable");
    attributes.put(UART_FLOW, "UART Flow Control");
    attributes.put(CTRL_POINT, "Control Point");
    attributes.put(UART_BAUD, "UART Baud Rate");
  }

  static String lookup(String uuid, String defaultName) {
    String name = attributes.get(uuid);
    return name == null ? defaultName : name;
  }
}
