package com.vivi.bluetoothlegatt.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.vivi.bluetoothlegatt.R;
import com.vivi.bluetoothlegatt.model.PresetModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class StaticDataUtility {

  public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
  public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

  public static String presetName = "";
  public static String myPreference = "visualvibe";
  public static String CURRENT_PRESET_MODEL = "currentPresetModel";
  public static String ANIMATION_CHANGE = "animationChange";
  public static String VALUE_CHANGE = "valueChange";
  //public static String ON_OFF_BUTTON = "on_off_button";

  public static ArrayList<String> generateCopyPresetName(String presetName) {

    ArrayList<String> presetNameList = new ArrayList<>();

    for (int i = 1; i < 100; i++) {
      presetNameList.add(presetName + "_" + i);
    }

    return presetNameList;
  }

  public static ArrayList<String> generatePresetName() {

    ArrayList<String> presetNameList = new ArrayList<>();

    for (int i = 1; i < 100; i++) {
      presetNameList.add("Preset" + i);
    }

    return presetNameList;
  }

  public static String readRawTextFile(Context context, int id) {
    try {
      Resources resources = context.getResources();
      InputStream inputStream = resources.openRawResource(id);
      BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
      StringBuilder text = new StringBuilder();
      String line = reader.readLine();
      while (line != null) {
        text.append(line);
        line = reader.readLine();
      }
      return text.toString();
    } catch (Exception e) {
      // e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<PresetModel> getPresetsFromConfig(Context context) {
    String json = readRawTextFile(context, R.raw.default_presets);
    if (json == null) {
      return new ArrayList<>();
    }

    try {
      Gson gson = new Gson();
      PresetModel[] presetModels = gson.fromJson(json, PresetModel[].class);
      return new ArrayList<>(Arrays.asList(presetModels));
    } catch (JsonParseException e) {
      Log.e("JSON Parser", "Error parsing data " + e.toString());
      return new ArrayList<>();
    }
  }
}
