/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vivi.bluetoothlegatt;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {

  private final static String TAG = DeviceControlActivity.class.getSimpleName();
  //fifos for each characteristic used
  private ArrayList<byte[]> ctrlfifo;
  private ArrayList<byte[]> txfifo;
  // private ArrayList<byte[]> rxfifo;
  private ArrayList<byte[]> enablefifo;
  private ArrayList<byte[]> flowfifo;
  private ArrayList<byte[]> baudfifo;
  private TextView mConnectionState;
  private TextView mDataField;
  private String mDeviceAddress;
  private ExpandableListView mGattServicesList;
  private BluetoothLeService mBluetoothLeService;
  // Code to manage Service lifecycle.
  private final ServiceConnection mServiceConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
      mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
      if (!mBluetoothLeService.initialize()) {
        Log.e(TAG, "Unable to initialize Bluetooth");
        finish();
      }
      // Automatically connects to the device upon successful start-up initialization.
      mBluetoothLeService.connect(mDeviceAddress);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
      mBluetoothLeService = null;
      displayData("Connection Failed");
    }
  };
  private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
      new ArrayList<>();
  private boolean mConnected = false;
  private BluetoothGattCharacteristic mNotifyCharacteristic;
  // If a given GATT characteristic is selected, check for supported features.  This sample
  // demonstrates 'Read' and 'Notify' features.  See
  // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
  // list of supported characteristic features.
  private final ExpandableListView.OnChildClickListener servicesListClickListner =
      new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
          if (mGattCharacteristics != null) {
            final BluetoothGattCharacteristic characteristic =
                mGattCharacteristics.get(groupPosition).get(childPosition);
            final int charaProp = characteristic.getProperties();
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
              // If there is an active notification on a characteristic, clear
              // it first so it doesn't update the data field on the user interface.
              if (mNotifyCharacteristic != null) {
                mBluetoothLeService.setCharacteristicNotification(
                    mNotifyCharacteristic, false);
                mNotifyCharacteristic = null;
              }
              mBluetoothLeService.readCharacteristic(characteristic);
            }
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
              mNotifyCharacteristic = characteristic;
              mBluetoothLeService.setCharacteristicNotification(
                  characteristic, true);
            }
            return true;
          }
          return false;
        }
      };
  private BluetoothGattCharacteristic txChar;
  // private BluetoothGattCharacteristic rxChar;
  private BluetoothGattCharacteristic enableChar;
  private BluetoothGattCharacteristic ctrlChar;
  private BluetoothGattCharacteristic flowChar;
  private BluetoothGattCharacteristic baudChar;
  //bootloader vars
  private int blstate;
  // Handles various events fired by the Service.
  // ACTION_GATT_CONNECTED: connected to a GATT server.
  // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
  // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
  // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
  //                        or notification operations.
  private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();
      if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
        mConnected = true;
        updateConnectionState();
        invalidateOptionsMenu();
      } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
        mConnected = false;
        //updateConnectionState(R.string.disconnected);
        invalidateOptionsMenu();
        clearUI();
        updateConnState("Connection Failed");
      } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
        // Show all the supported services and characteristics on the user interface.
        displayGattServices(mBluetoothLeService.getSupportedGattServices());
      } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
        displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
        Log.d(TAG, "received:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
      } else if (BluetoothLeService.ACTION_DATA_TX.equals(action)) {
        Log.d(TAG, "tx complete");
        if (blstate > 0) {
          nextBlMsg();
        }
      }
    }
  };

  private static IntentFilter makeGattUpdateIntentFilter() {
    final IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_TX);
    return intentFilter;
  }

  private void clearUI() {
    mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
    mDataField.setText(R.string.no_data);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.gatt_services_characteristics);
    final Intent intent = getIntent();
    String mDeviceName = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_NAME);
    mDeviceAddress = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_ADDRESS);

    // Sets up UI references.
    ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
    mGattServicesList = findViewById(R.id.gatt_services_list);
    mGattServicesList.setOnChildClickListener(servicesListClickListner);
    mConnectionState = findViewById(R.id.connection_state);
    mDataField = findViewById(R.id.data_value);

    if (getActionBar() != null) {
      getActionBar().setTitle(mDeviceName);
      getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    ctrlfifo = new ArrayList<>();
    txfifo = new ArrayList<>();
    // rxfifo = new ArrayList<byte[]>();
    enablefifo = new ArrayList<>();
    flowfifo = new ArrayList<>();
    baudfifo = new ArrayList<>();

    Button b_txtest = this.findViewById(R.id.b_txtest);
    b_txtest.setOnClickListener(v -> {
      //TextView colorObject = (TextView) findViewById(R.id.textViewColorBass);
      Log.d(TAG, "txtest button pressed");
      startBootload();
/*
              if(teststate == 0) {
                  byte[] writeval = {0x37, 0x00};
                  mBluetoothLeService.writeCharacteristic(txChar.getUuid().toString(), writeval);
                  teststate = 1;
                  byte[] cfggpiobytes = {0x50, 0x07, 0x01, 0x00}; //gpio config command, pin map 5, output, no pull
                  mBluetoothLeService.writeChar(ctrlChar, cfggpiobytes);
              } else if(teststate == 1) {
                  //byte[] writeval = {0x38, 0x00};
                  //mBluetoothLeService.writeCharacteristic(txChar.getUuid().toString(), writeval);
                  byte[] gpioset = {0x51, 0x07, 0x01};
                  mBluetoothLeService.writeChar(ctrlChar, gpioset);
                  Log.d(TAG, "set reset pin");
                  teststate = 2;
              } else if(teststate == 2) {
                  //byte[] writeval = {0x39, 0x00};
                  //mBluetoothLeService.writeCharacteristic(txChar.getUuid().toString(), writeval);
                  byte[] gpioclr = {0x51, 0x07, 0x00};
                  mBluetoothLeService.writeChar(ctrlChar, gpioclr);
                  Log.d(TAG, "clear reset pin");
                  teststate = 1;
              }
*/

    });

    updateConnState("Connecting...");
  }

  private void startBootload() {
    Log.d(TAG, "startBootload");
    ctrlfifo.clear();
    txfifo.clear();
    // rxfifo.clear();
    enablefifo.clear();
    baudfifo.clear();
    flowfifo.clear();
    blstate = 0;
    //stk500 protocol vars

    flowfifo.add(new byte[] { 0x01 });

    //enablefifo.add(new byte[]{0x00});
    enablefifo.add(new byte[] { 0x01 });

    baudfifo.add(new byte[] { 0x00, 0x01, (byte) 0xC2, 0x00 });
    //baudfifo.add(new byte[]{0x00, 0x00, (byte)0xE1, 0x00});

    ctrlfifo.add(new byte[] { 0x50, 0x05, 0x01, 0x00 });
    ctrlfifo.add(new byte[] { 0x51, 0x05, 0x01 });
    ctrlfifo.add(new byte[] { 0x51, 0x05, 0x00 }); //0x05 for correct pin, 0x07 for p0.15
    ctrlfifo.add(new byte[] { 0x51, 0x05, 0x01 }); //toggle reset line off and on p0.15

    txfifo.add(new byte[] { 0x1B, 0x01, 0x00, 0x01, 0x0E, 0x01, 0x14 });
    blstate = 1;
    nextBlMsg();
  }

  //look at ctrlFifo and txFifo, send from ctrlfifo or if it is empty send from txfifo
  private void nextBlMsg() {
    byte[] thismessage;
    if (blstate == 1) {
      thismessage = flowfifo.remove(0);
      mBluetoothLeService.writeChar(flowChar, thismessage);
      blstate = 2;
    } else if (blstate == 2) {
      thismessage = baudfifo.remove(0);
      mBluetoothLeService.writeChar(baudChar, thismessage);
      blstate = 3;
    } else if (blstate == 3) {
      thismessage = enablefifo.remove(0);
      mBluetoothLeService.writeChar(enableChar, thismessage);
      blstate = 4;
    } else if (blstate == 4) {
      thismessage = ctrlfifo.remove(0);
      mBluetoothLeService.writeChar(ctrlChar, thismessage);
      if (ctrlfifo.size() == 0) {
        blstate = 5;
        try {
          TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
          Log.e(TAG, "sleep error");
        }
      }
    } else if (blstate == 5) {
      thismessage = txfifo.remove(0);
      mBluetoothLeService.writeChar(txChar, thismessage);
      if (txfifo.size() == 0) {
        blstate = 0;
      }
    } else {
      thismessage = "no message".getBytes();
    }
    Log.d(TAG, "nextBleMsg:" + new String(thismessage));
  }

  @Override
  protected void onResume() {
    super.onResume();
    registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    if (mBluetoothLeService != null) {
      final boolean result = mBluetoothLeService.connect(mDeviceAddress);
      Log.d(TAG, "Connect request result=" + result);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mGattUpdateReceiver);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    unbindService(mServiceConnection);
    mBluetoothLeService = null;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.gatt_services, menu);
    if (mConnected) {
      menu.findItem(R.id.menu_connect).setVisible(false);
      menu.findItem(R.id.menu_disconnect).setVisible(true);
    } else {
      menu.findItem(R.id.menu_connect).setVisible(true);
      menu.findItem(R.id.menu_disconnect).setVisible(false);
    }
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_connect:
        mBluetoothLeService.connect(mDeviceAddress);
        updateConnState("Connecting...");
        return true;
      case R.id.menu_disconnect:
        mBluetoothLeService.disconnect();
        return true;
      case android.R.id.home:
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void updateConnectionState() {
    runOnUiThread(() -> mConnectionState.setText(R.string.connected));
  }

  private void updateConnState(final String val) {
    runOnUiThread(() -> mConnectionState.setText(val));
  }

  private void displayData(String data) {
    if (data != null) {
      mDataField.setText(data);
    }
  }

  // Demonstrates how to iterate through the supported GATT Services/Characteristics.
  // In this sample, we populate the data structure that is bound to the ExpandableListView
  // on the UI.
  private void displayGattServices(List<BluetoothGattService> gattServices) {
    if (gattServices == null) return;
    String uuid;
    String unknownServiceString = getResources().getString(R.string.unknown_service);
    String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
    ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<>();
    ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
        = new ArrayList<>();
    mGattCharacteristics = new ArrayList<>();

    // Loops through available GATT Services.
    String LIST_NAME = "NAME";
    String LIST_UUID = "UUID";
    for (BluetoothGattService gattService : gattServices) {
      HashMap<String, String> currentServiceData = new HashMap<>();
      uuid = gattService.getUuid().toString();
      Log.d(TAG, "service uuid " + uuid);
      currentServiceData.put(
          LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
      currentServiceData.put(LIST_UUID, uuid);
      gattServiceData.add(currentServiceData);

      ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
          new ArrayList<>();
      List<BluetoothGattCharacteristic> gattCharacteristics =
          gattService.getCharacteristics();
      ArrayList<BluetoothGattCharacteristic> charas =
          new ArrayList<>();

      // Loops through available Characteristics.
      for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
        charas.add(gattCharacteristic);
        HashMap<String, String> currentCharaData = new HashMap<>();
        uuid = gattCharacteristic.getUuid().toString();
        Log.d(TAG, "chara uuid " + uuid);
        currentCharaData.put(
            LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
        currentCharaData.put(LIST_UUID, uuid);
        gattCharacteristicGroupData.add(currentCharaData);

        if (uuid.equals(SampleGattAttributes.UART_RX)) {
          txChar = gattCharacteristic;
          Log.d(TAG, "found rx characteristic");
          //mBluetoothLeService.setCharacteristicNotification(rxChar, true);

        }
        if (uuid.equals(SampleGattAttributes.UART_TX)) {
          txChar = gattCharacteristic;
          Log.d(TAG, "found tx characteristic");
          txChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          //mBluetoothLeService.setCharacteristicNotification(txChar, true);

        }
        if (uuid.equals(SampleGattAttributes.UART_ENABLE)) {
          //byte[] writeval = {0x01};
          enableChar = gattCharacteristic;
          enableChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found enable characteristic");
          //mBluetoothLeService.writeChar(enableChar, new byte[]{0x01});
          //mBluetoothLeService.setCharacteristicNotification(enableChar, true);

        }
        if (uuid.equals(SampleGattAttributes.CTRL_POINT)) {
          ctrlChar = gattCharacteristic;
          ctrlChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
          Log.d(TAG, "found control point characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_FLOW)) {
          flowChar = gattCharacteristic;
          flowChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found flow characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_BAUD)) {
          baudChar = gattCharacteristic;
          baudChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found baud characteristic");
          //mBluetoothLeService.writeChar(gattCharacteristic, new byte[]{0x00, 0x01, (byte)0xc2, 0x00});
        }
      }
      mGattCharacteristics.add(charas);
      gattCharacteristicData.add(gattCharacteristicGroupData);
    }

    SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
        this,
        gattServiceData,
        android.R.layout.simple_expandable_list_item_2,
        new String[] { LIST_NAME, LIST_UUID },
        new int[] { android.R.id.text1, android.R.id.text2 },
        gattCharacteristicData,
        android.R.layout.simple_expandable_list_item_2,
        new String[] { LIST_NAME, LIST_UUID },
        new int[] { android.R.id.text1, android.R.id.text2 }
    );
    mGattServicesList.setAdapter(gattServiceAdapter);
  }
}
