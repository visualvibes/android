package com.vivi.bluetoothlegatt;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.Nullable;
import com.vivi.bluetoothlegatt.database.DatabaseHandler;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;

public class BaseActivity extends Activity {

  public DatabaseHandler databaseHandler;
  public SharedPreferences sharedpreferences;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    databaseHandler = new DatabaseHandler(this);

    sharedpreferences = getSharedPreferences(StaticDataUtility.myPreference,
        Context.MODE_PRIVATE);
  }

  public DatabaseHandler getDatabaseHandler() {
    return databaseHandler;
  }

  public void showToast(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  //public void showErrorSnackBar(@NonNull Context context, View view, String message) {
  //  Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
  //  View snackBarView = snackbar.getView();
  //  snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color
  //      .colorSnackBarNegative));
  //  TextView textView = snackBarView.findViewById(R.id.snackbar_text);
  //  textView.setTextColor(Color.WHITE);
  //  snackbar.show();
  //}
  //
  //public void showSuccessSnackBar(@NonNull Context context, View view, String message) {
  //  Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
  //  View snackBarView = snackbar.getView();
  //  snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color
  //      .colorSnackBarPositive));
  //  TextView textView = snackBarView.findViewById(R.id.snackbar_text);
  //  textView.setTextColor(Color.WHITE);
  //  snackbar.show();
  //}
}
