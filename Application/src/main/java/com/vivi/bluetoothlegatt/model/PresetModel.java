package com.vivi.bluetoothlegatt.model;

public class PresetModel {

  private int id;
  private int presetId;
  private String presetName;
  private int bassColor;
  private int bassBright;
  private int bassColorOptions;
  private int bassAnimationOptions;
  private int bassAutoAnimation;

  private int midColor;
  private int midBright;
  private int midColorOptions;
  private int midAnimationOptions;
  private int midAutoAnimation;

  private int trebleColor;
  private int trebleBright;
  private int trebleColorOptions;
  private int trebleAnimationOptions;
  private int trebleAutoAnimation;
  private boolean isPresetOn;

  @Override
  public boolean equals(Object object) {
    if(this == object)
      return true;

    if(object == null || object.getClass() != this.getClass()) {
      return false;
    }

    PresetModel preset = (PresetModel) object;
    return preset.presetName.equalsIgnoreCase(this.presetName);
  }

  @Override
  public int hashCode() {
    return presetName.hashCode();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPresetId() {
    return presetId;
  }

  public void setPresetId(int presetId) {
    this.presetId = presetId;
  }

  public String getPresetName() {
    return presetName;
  }

  public void setPresetName(String presetName) {
    this.presetName = presetName;
  }

  public int getBassColor() {
    return bassColor;
  }

  public void setBassColor(int bassColor) {
    this.bassColor = bassColor;
  }

  public int getBassBright() {
    return bassBright;
  }

  public void setBassBright(int bassBright) {
    this.bassBright = bassBright;
  }

  public int getBassColorOptions() {
    return bassColorOptions;
  }

  public void setBassColorOptions(int bassColorOptions) {
    this.bassColorOptions = bassColorOptions;
  }

  public int getBassAnimationOptions() {
    return bassAnimationOptions;
  }

  public void setBassAnimationOptions(int bassAnimationOptions) {
    this.bassAnimationOptions = bassAnimationOptions;
  }

  public int getBassAutoAnimation() {
    return bassAutoAnimation;
  }

  public void setBassAutoAnimation(int bassAutoAnimation) {
    this.bassAutoAnimation = bassAutoAnimation;
  }

  public int getMidColor() {
    return midColor;
  }

  public void setMidColor(int midColor) {
    this.midColor = midColor;
  }

  public int getMidBright() {
    return midBright;
  }

  public void setMidBright(int midBright) {
    this.midBright = midBright;
  }

  public int getMidColorOptions() {
    return midColorOptions;
  }

  public void setMidColorOptions(int midColorOptions) {
    this.midColorOptions = midColorOptions;
  }

  public int getMidAnimationOptions() {
    return midAnimationOptions;
  }

  public void setMidAnimationOptions(int midAnimationOptions) {
    this.midAnimationOptions = midAnimationOptions;
  }

  public int getMidAutoAnimation() {
    return midAutoAnimation;
  }

  public void setMidAutoAnimation(int midAutoAnimation) {
    this.midAutoAnimation = midAutoAnimation;
  }

  public int getTrebleColor() {
    return trebleColor;
  }

  public void setTrebleColor(int trebleColor) {
    this.trebleColor = trebleColor;
  }

  public int getTrebleBright() {
    return trebleBright;
  }

  public void setTrebleBright(int trebleBright) {
    this.trebleBright = trebleBright;
  }

  public int getTrebleColorOptions() {
    return trebleColorOptions;
  }

  public void setTrebleColorOptions(int trebleColorOptions) {
    this.trebleColorOptions = trebleColorOptions;
  }

  public int getTrebleAnimationOptions() {
    return trebleAnimationOptions;
  }

  public void setTrebleAnimationOptions(int trebleAnimationOptions) {
    this.trebleAnimationOptions = trebleAnimationOptions;
  }

  public int getTrebleAutoAnimation() {
    return trebleAutoAnimation;
  }

  public void setTrebleAutoAnimation(int trebleAutoAnimation) {
    this.trebleAutoAnimation = trebleAutoAnimation;
  }

  public boolean isPresetOn() {
    return isPresetOn;
  }

  public void setPresetOn(boolean presetOn) {
    isPresetOn = presetOn;
  }
}
