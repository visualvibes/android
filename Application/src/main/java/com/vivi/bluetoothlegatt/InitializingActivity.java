package com.vivi.bluetoothlegatt;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.vivi.bluetoothlegatt.database.TablePresetMaster;
import com.vivi.bluetoothlegatt.model.PresetModel;
import com.vivi.bluetoothlegatt.utils.StaticDataUtility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class InitializingActivity extends BaseActivity {

  private final static String TAG = "ViviLink Init";
  private String mDeviceAddress;
  private ProgressBar initProgressBar;
  private TextView progressMessage;
  private BluetoothGattCharacteristic txChar;
  private BluetoothGattCharacteristic rxChar;
  private BluetoothGattCharacteristic enableChar;
  private BluetoothGattCharacteristic ctrlChar;
  private BluetoothGattCharacteristic flowChar;
  private BluetoothGattCharacteristic baudChar;

  //fifos for each characteristic used
  private ArrayList<byte[]> ctrlfifo;
  private ArrayList<byte[]> txfifo;
  // private ArrayList<byte[]> rxfifo;
  private ArrayList<byte[]> enablefifo;
  private ArrayList<byte[]> flowfifo;
  private ArrayList<byte[]> baudfifo;

  private SharedPreferences.Editor prefsEditor;

  //saved values
  private int basscolor = 0; //element 4
  private int bassbrightness = 0; //7
  private int bassoptions = 0; //10
  private int bassanimation = 0; //1
  private int bassauto = 0; //11
  private int midcolor = 0; //5
  private int midbrightness = 0; //8
  private int midoptions = 0; //12
  private int midanimation = 0; //2
  private int midauto = 0; //13
  private int treblecolor = 0; //6
  private int treblebrightness = 0; //9
  private int trebleoptions = 0; //14
  private int trebleanimation = 0; //3
  private int trebleauto = 0; //15
  private int filter = 0; //0
  private int treblecount = 0; //21
  private int midcount = 0; //19
  private int basscount = 0; //17
  private String mDeviceName;
  private int receivesettings;
  //private ExpandableListView mGattServicesList;
  private BluetoothLeService mBluetoothLeService;
  // Code to manage Service lifecycle.
  private final ServiceConnection mServiceConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
      mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
      if (!mBluetoothLeService.initialize()) {
        Log.e(TAG, "Unable to initialize Bluetooth");
        progressMessage.setText(getString(R.string.str_unable_initialize_bluetooth));
        finish();
      }
      // Automatically connects to the device upon successful start-up initialization.
      mBluetoothLeService.connect(mDeviceAddress);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
      mBluetoothLeService = null;
      progressMessage.setText(getString(R.string.str_connection_failed));
    }
  };

  //private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
  //    new ArrayList<>();

  //private boolean mConnected = false;
  private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();
      if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
        //mConnected = true;
        progressMessage.setText(getString(R.string.str_connected));
        initProgressBar.setProgress(50);
      } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
        //mConnected = false;
        progressMessage.setText(getString(R.string.str_connection_disconnected));

        //deviceTabbedScreen();

      } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
        // Show all the supported services and characteristics on the user interface.
        progressMessage.setText(getString(R.string.str_services_discovered));
        initProgressBar.setProgress(75);
        searchGattServices(mBluetoothLeService.getSupportedGattServices());

        //deviceTabbedScreen();
      } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
        //displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
        String pstring = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
        //byte[] packet = pstring.getBytes(); //wrong!
        if (receivesettings > 0) {
          int numbytes = pstring.length() / 2;
          Log.d(TAG, "received:" + pstring);
          for (int i = 0; i < numbytes; i++) {
            if (receivesettings <= 0) { //safety check
              break;
            }
            int index = 22 - receivesettings;
            switch (index) {
              case 0: //filter
                filter = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("filter: %d", filter));
                break;
              case 1: //bass animation
                bassanimation = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("bassanimation: %d", bassanimation));
                break;
              case 2:
                midanimation = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midanimation: %d", midanimation));
                break;
              case 3:
                trebleanimation = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("trebleanimation: %d", trebleanimation));
                break;
              case 4:
                basscolor = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("basscolor: %d", basscolor));
                break;
              case 5:
                midcolor = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midcolor: %d", midcolor));
                break;
              case 6:
                treblecolor = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("treblecolor: %d", treblecolor));
                break;
              case 7:
                bassbrightness = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("bassbrightness: %d", bassbrightness));
                break;
              case 8:
                midbrightness = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midbrightness: %d", midbrightness));
                break;
              case 9:
                treblebrightness = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("treblebrightness: %d", treblebrightness));
                break;
              case 10:
                bassoptions = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("bassoptions: %d", bassoptions));
                break;
              case 11:
                bassauto = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("bassauto: %d", bassauto));
                break;
              case 12:
                midoptions = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midoptions: %d", midoptions));
                break;
              case 13:
                midauto = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midauto: %d", midauto));
                break;
              case 14:
                trebleoptions = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("trebleoptions: %d", trebleoptions));
                break;
              case 15:
                trebleauto = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("trebleauto: %d", trebleauto));
                break;
              case 16: //basscount H
                basscount = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16) * 256;
                Log.d(TAG, String.format("basscount H: %d", basscount));
                break;
              case 17: //basscount L
                basscount += Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("basscount H+L: %d", basscount));
                break;
              case 18: //midcount H
                midcount = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16) * 256;
                Log.d(TAG, String.format("midcount H: %d", midcount));
                break;
              case 19: //midcount L
                midcount += Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("midcount H+L: %d", midcount));
                break;
              case 20: //treblecount H
                treblecount = Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16) * 256;
                Log.d(TAG, String.format("treblecount H: %d", treblecount));
                break;
              case 21: //treblecount L
                treblecount += Integer.parseInt(pstring.substring(i * 2, i * 2 + 2), 16);
                Log.d(TAG, String.format("treblecount H+L: %d", treblecount));
                break;
            }
            receivesettings -= 1;
            if (receivesettings == 0) {
              deviceTabbedScreen();
            }
          }
                    /*
                    //assign the 20 bytes to settings vars
                    filter = Integer.parseInt(pstring.substring(0,2), 16);
                    bassanimation = Integer.parseInt(pstring.substring(2,4), 16); //packet[1] & 0xff;
                    midanimation = Integer.parseInt(pstring.substring(4,6), 16); //packet[2] & 0xff;
                    trebleanimation = Integer.parseInt(pstring.substring(6,8), 16); //packet[3] & 0xff;
                    Log.d(TAG, String.format("filter: %d bassanimation: %d midanimation: %d trebleanimation: %d", filter, bassanimation, midanimation, trebleanimation));
                    basscolor = Integer.parseInt(pstring.substring(8,10), 16); //packet[4] & 0xff;
                    midcolor = Integer.parseInt(pstring.substring(10,12), 16); //packet[5] & 0xff;
                    treblecolor = Integer.parseInt(pstring.substring(12,14), 16); //packet[6] & 0xff;
                    Log.d(TAG, String.format("basscolor: %d midcolor: %d treblecolor: %d", basscolor, midcolor, treblecolor));
                    bassbrightness = Integer.parseInt(pstring.substring(14,16), 16); //packet[7] & 0xff;
                    midbrightness = Integer.parseInt(pstring.substring(16,18), 16); //packet[8] & 0xff;
                    treblebrightness = Integer.parseInt(pstring.substring(18,20), 16); //packet[9] & 0xff;
                    Log.d(TAG, String.format("bassbright: %d midbright: %d treblebright: %d", bassbrightness, midbrightness, treblebrightness));
                    bassoptions = Integer.parseInt(pstring.substring(20,22), 16); //packet[10] & 0xff;
                    bassauto = Integer.parseInt(pstring.substring(22,24), 16); //packet[11] & 0xff;
                    Log.d(TAG, String.format("bassoptions: %d bassauto: %d", bassoptions, bassauto));
                    midoptions = Integer.parseInt(pstring.substring(24,26), 16); //packet[12] & 0xff;
                    midauto = Integer.parseInt(pstring.substring(26,28), 16); //packet[13] & 0xff; // this returns the bass setting
                    Log.d(TAG, String.format("midoptions: %d midauto: %d", midoptions, midauto));
                    trebleoptions = Integer.parseInt(pstring.substring(28,30), 16); //packet[14] & 0xff;
                    trebleauto = Integer.parseInt(pstring.substring(30,32), 16); //packet[15] & 0xff; //this returns mid color setting
                    Log.d(TAG, String.format("trebleoptions: %d trebleauto: %d", trebleoptions, trebleauto));
                    //bassprotocol = Integer.parseInt(pstring.substring(48,50), 16); //packet[16] & 0xff; //this returns bass animation options
                    basscount = Integer.parseInt(pstring.substring(32,36), 16); //packet[17] & 0xff;
                    Log.d(TAG, String.format("basscount: %d", basscount));
                    //midprotocol = Integer.parseInt(pstring.substring(54,56), 16); //packet[18] & 0xff;
                    //midcount = Integer.parseInt(pstring.substring(36,40), 16); //packet[19] & 0xff;
                    //Log.d(TAG, String.format("midcount: %d", midcount));
                //} else if(receivesettings == 1) {
                    Log.d(TAG, "received1:" + pstring);
                    //final 4 bytes of settings
                    midcount = Integer.parseInt(pstring.substring(0,4), 16); //packet[19] & 0xff;
                    Log.d(TAG, String.format("midcount: %d", midcount));
                    //trebleprotocol = Integer.parseInt(pstring.substring(0,2), 16); //packet[0] & 0xff;
                    treblecount = Integer.parseInt(pstring.substring(4,8), 16); //packet[1] & 0xff;
                    Log.d(TAG, String.format("treblecount: %d", treblecount));
                    receivesettings = 0;

                    deviceTabbedScreen(); */
        } else {
          Log.d(TAG, "received:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
        }
      } else if (BluetoothLeService.ACTION_DATA_TX.equals(action)) {
        Log.d(TAG, "tx complete");
        nextBleMsg();
      }
    }
  };

  private static IntentFilter makeGattUpdateIntentFilter() {
    final IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
    intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
    intentFilter.addAction(BluetoothLeService.ACTION_DATA_TX);
    return intentFilter;
  }

  public void onBackPressed() {
    Log.d(TAG, "go back to home screen");

    //Intent intent = new Intent(this, DeviceScanActivity.class);
    mBluetoothLeService.disconnect();

    finish();
    //startActivity(intent);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_initializing);
    initProgressBar = findViewById(R.id.initProgressBar);
    initProgressBar.setProgress(25);
    progressMessage = findViewById(R.id.progressMessage);

    final Intent intent = getIntent();
    mDeviceName = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_NAME);
    mDeviceAddress = intent.getStringExtra(StaticDataUtility.EXTRAS_DEVICE_ADDRESS);

    Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    ctrlfifo = new ArrayList<>();
    txfifo = new ArrayList<>();
    // rxfifo = new ArrayList<>();
    enablefifo = new ArrayList<>();
    flowfifo = new ArrayList<>();
    baudfifo = new ArrayList<>();

    prefsEditor = sharedpreferences.edit();
    prefsEditor.putString(StaticDataUtility.VALUE_CHANGE, "no");
    prefsEditor.apply();

    syncDefaultPresets(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    if (mBluetoothLeService != null) {
      final boolean result = mBluetoothLeService.connect(mDeviceAddress);
      Log.d(TAG, "Connect request result=" + result);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.d(TAG, "onPause()");
    unregisterReceiver(mGattUpdateReceiver);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy");
    unbindService(mServiceConnection);
    mBluetoothLeService = null;
  }

  //look at ctrlFifo and txFifo, send from ctrlfifo or if it is empty send from txfifo
  private void nextBleMsg() {
    if (mBluetoothLeService == null) {
      return;
    }
    byte[] thismessage;
    if (flowfifo.size() > 0) {
      thismessage = flowfifo.remove(0);
      mBluetoothLeService.writeChar(flowChar, thismessage);
      Log.d(TAG, "write to flowChar");
    } else if (baudfifo.size() > 0) {
      thismessage = baudfifo.remove(0);
      mBluetoothLeService.writeChar(baudChar, thismessage);
      Log.d(TAG, "write to baudChar");
    } else if (enablefifo.size() > 0) {
      thismessage = enablefifo.remove(0);
      mBluetoothLeService.writeChar(enableChar, thismessage);
      Log.d(TAG, "write to enableChar");
    } else if (ctrlfifo.size() > 0) {
      thismessage = ctrlfifo.remove(0);
      mBluetoothLeService.writeChar(ctrlChar, thismessage);
      Log.d(TAG, "write to ctrlChar");
      //todo: wait 300ms if in bootloader after a reset toggle
    } else if (txfifo.size() > 0) {
      thismessage = txfifo.remove(0);
      mBluetoothLeService.writeChar(txChar, thismessage);
      Log.d(TAG, "write to txChar");
    } else {
      Log.d(TAG, "ble xmit done");
      progressMessage.setText(getString(R.string.str_complete));
      initProgressBar.setProgress(100);

      return;
      //thismessage = "no message".getBytes();
    }
    Log.d(TAG, "nextBleMsg:" + Arrays.toString(thismessage));
  }

  private void deviceTabbedScreen() {
    Log.i(TAG, "starting tab activity");

    StaticDataUtility.presetName = "";
    prefsEditor.putString("presetName", StaticDataUtility.presetName);
    prefsEditor.apply();

    final Intent nextIntent = new Intent(this, TabbedActivity.class);

    nextIntent.putExtra("DEVICE_NAME", mDeviceName);
    nextIntent.putExtra("DEVICE_ADDRESS", mDeviceAddress);
    nextIntent.putExtra("basscolor", basscolor);
    nextIntent.putExtra("bassbrightness", bassbrightness);
    Log.d(TAG, "putting into intent bass color "
        + basscolor
        + " brightness "
        + bassbrightness
        + " options "
        + bassoptions);

    if (bassoptions > 7) {
      bassoptions = 0;
      nextIntent.putExtra("bassoptions", bassoptions);
    } else {
      nextIntent.putExtra("bassoptions", bassoptions);
    }

    nextIntent.putExtra("bassanimation", bassanimation);
    nextIntent.putExtra("bassauto", bassauto);
    nextIntent.putExtra("midcolor", midcolor);
    nextIntent.putExtra("midbrightness", midbrightness);
    Log.d(TAG, "putting into intent mid color "
        + midcolor
        + " brightness "
        + midbrightness
        + " options "
        + midoptions);

    if (midoptions > 7) {
      midoptions = 0;
      nextIntent.putExtra("midoptions", midoptions);
    } else {
      nextIntent.putExtra("midoptions", midoptions);
    }

    nextIntent.putExtra("midanimation", midanimation);
    nextIntent.putExtra("midauto", midauto);
    nextIntent.putExtra("treblecolor", treblecolor);
    nextIntent.putExtra("treblebrightness", treblebrightness);
    Log.d(TAG, "putting into intent treble color "
        + treblecolor
        + " brightness "
        + treblebrightness
        + " options "
        + trebleoptions);

    if (trebleoptions > 7) {
      trebleoptions = 0;
      nextIntent.putExtra("trebleoptions", trebleoptions);
    } else {
      nextIntent.putExtra("trebleoptions", trebleoptions);
    }

    nextIntent.putExtra("trebleanimation", trebleanimation);
    nextIntent.putExtra("trebleauto", trebleauto);
    nextIntent.putExtra("filter", filter);
    nextIntent.putExtra("treblecount", treblecount);
    //20
    int trebleprotocol = 0;
    nextIntent.putExtra("trebleprotocol", trebleprotocol);
    nextIntent.putExtra("midcount", midcount);
    //18
    int midprotocol = 0;
    nextIntent.putExtra("midprotocol", midprotocol);
    nextIntent.putExtra("basscount", basscount);
    //16
    int bassprotocol = 0;
    nextIntent.putExtra("bassprotocol", bassprotocol);
    startActivity(nextIntent);
    setDataToPref();
    finish();
  }

  private void setDataToPref() {

    PresetModel presetModel = new PresetModel();
    presetModel.setBassColor(basscolor);
    presetModel.setBassBright(bassbrightness);
    presetModel.setBassColorOptions(bassoptions);
    presetModel.setBassAnimationOptions(bassanimation - 9);
    presetModel.setBassAutoAnimation(bassauto);

    presetModel.setMidColor(midcolor);
    presetModel.setMidBright(midbrightness);
    presetModel.setMidColorOptions(midoptions);
    presetModel.setMidAnimationOptions(midanimation - 9);
    presetModel.setMidAutoAnimation(midauto);

    presetModel.setTrebleColor(treblecolor);
    presetModel.setTrebleBright(treblebrightness);
    presetModel.setTrebleColorOptions(trebleoptions);
    presetModel.setTrebleAnimationOptions(trebleanimation - 9);
    presetModel.setTrebleAutoAnimation(trebleauto);

    Gson gson = new Gson();
    prefsEditor.putString(StaticDataUtility.CURRENT_PRESET_MODEL, gson.toJson(presetModel));
    prefsEditor.apply();
  }

  private void searchGattServices(List<BluetoothGattService> gattServices) {
    if (gattServices == null) return;
    String uuid;

    //String unknownServiceString = getResources().getString(R.string.unknown_service);
    //String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
    //ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<>();
    //ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
    //    = new ArrayList<>();

    //mGattCharacteristics = new ArrayList<>();

    // Loops through available GATT Services.
    for (BluetoothGattService gattService : gattServices) {
      //HashMap<String, String> currentServiceData = new HashMap<>();
      uuid = gattService.getUuid().toString();
      Log.d(TAG, "service uuid " + uuid);
      //currentServiceData.put("NAME", SampleGattAttributes.lookup(uuid, unknownServiceString));
      //currentServiceData.put("UUID", uuid);
      //gattServiceData.add(currentServiceData);

      //ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
      //    new ArrayList<>();
      List<BluetoothGattCharacteristic> gattCharacteristics =
          gattService.getCharacteristics();
      //ArrayList<BluetoothGattCharacteristic> charas =
      //    new ArrayList<>();

      // Loops through available Characteristics.
      for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
        //charas.add(gattCharacteristic);
        //HashMap<String, String> currentCharaData = new HashMap<>();
        uuid = gattCharacteristic.getUuid().toString();
        Log.d(TAG, "chara uuid " + uuid);
        //currentCharaData.put("NAME", SampleGattAttributes.lookup(uuid, unknownCharaString));
        //currentCharaData.put("UUID", uuid);

        //gattCharacteristicGroupData.add(currentCharaData);

        if (uuid.equals(SampleGattAttributes.UART_RX)) {
          rxChar = gattCharacteristic;
          Log.d(TAG, "found rx characteristic");
          mBluetoothLeService.setCharacteristicNotification(rxChar, true);
        }
        if (uuid.equals(SampleGattAttributes.UART_TX)) {
          txChar = gattCharacteristic;
          Log.d(TAG, "found tx characteristic");
          txChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          //mBluetoothLeService.setCharacteristicNotification(txChar, true);

        }
        if (uuid.equals(SampleGattAttributes.UART_ENABLE)) {
          //byte[] writeval = {0x01};
          enableChar = gattCharacteristic;
          enableChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found enable characteristic");
          //mBluetoothLeService.writeChar(enableChar, new byte[]{0x01});
          //mBluetoothLeService.setCharacteristicNotification(enableChar, true);

        }
        if (uuid.equals(SampleGattAttributes.CTRL_POINT)) {
          ctrlChar = gattCharacteristic;
          ctrlChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
          Log.d(TAG, "found control point characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_FLOW)) {
          flowChar = gattCharacteristic;
          flowChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found flow characteristic");
        }
        if (uuid.equals(SampleGattAttributes.UART_BAUD)) {
          baudChar = gattCharacteristic;
          baudChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

          Log.d(TAG, "found baud characteristic");
          //mBluetoothLeService.writeChar(gattCharacteristic, new byte[]{0x00, 0x01, (byte)0xc2, 0x00});
        }
      }
      //mGattCharacteristics.add(charas);
      //gattCharacteristicData.add(gattCharacteristicGroupData);
    }

    if (rxChar != null
        && txChar != null
        && enableChar != null
        && ctrlChar != null
        && flowChar != null
        && baudChar != null) {
      progressMessage.setText(getString(R.string.str_getting_settings));
      initProgressBar.setProgress(85);
      Log.d(TAG, "ble init");
      ctrlfifo.clear();
      txfifo.clear();
      // rxfifo.clear();
      enablefifo.clear();
      baudfifo.clear();
      flowfifo.clear();
      //stk500seq = 1;

      flowfifo.add(new byte[] { 0x01 });

      //enablefifo.add(new byte[]{0x00});
      enablefifo.add(new byte[] { 0x01 });

      baudfifo.add(new byte[] { 0x00, 0x01, (byte) 0xC2, 0x00 });
      txfifo.add(new byte[] { (byte) 0xff, (byte) 0xff, (byte) 0xff });
      receivesettings = 22; //waiting for 22 bytes
      try {
        TimeUnit.MILLISECONDS.sleep(300);
      } catch (InterruptedException e) {
        Log.e(TAG, "sleep error");
      }
      nextBleMsg();
    } else {
      String missing = String.format(Locale.getDefault(),
          "rxchar: %d, txchar: %d, enablechar: %d, ctrlchar: %d, flowchar: %d, baudchar: %d",
          rxChar == null ? 0 : 1, txChar == null ? 0 : 1, enableChar == null ? 0 : 1,
          ctrlChar == null ? 0 : 1, flowChar == null ? 0 : 1, baudChar == null ? 0 : 1);
      System.out.println("ViviLink missing --> " + missing);
      progressMessage.setText(getString(R.string.str_vivi_characteristics_not_found));
    }
  }

  private void syncDefaultPresets(Context context) {
    TablePresetMaster presetMaster = new TablePresetMaster(databaseHandler);
    ArrayList<PresetModel> currentPresets = presetMaster.getAllPresetData(sharedpreferences);
    ArrayList<PresetModel> configPresets = StaticDataUtility.getPresetsFromConfig(context);

    for (int i = 0; i < configPresets.size(); i++) {
      try {
        if (!currentPresets.contains(configPresets.get(i))) {
          presetMaster.insertData(configPresets.get(i));
        } else {
          presetMaster.updatePresetData(configPresets.get(i));
        }
      } catch(Exception e) {
        Log.e(TAG, "Error loading preset " + configPresets.get(i).getPresetId());
      }
    }
  }
}
